package fr.karmaowner.common;

import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitScheduler;
import org.bukkit.scheduler.BukkitTask;

public class TaskCreator{
	private BukkitRunnable run;
	private int taskId;
	private boolean running;
	private long DelayBeforeStart, RepeatingTime;
	
	public TaskCreator(BukkitRunnable r, long DelayBeforeStart, long RepeatingTime) {
		run = r;
		running = true;
		this.DelayBeforeStart = DelayBeforeStart;
		this.RepeatingTime = RepeatingTime;
		start();
	}
	
	public BukkitRunnable getRunnable() {
		return run;
	}
	
	public void start() {
		run.runTaskTimer(Main.INSTANCE, DelayBeforeStart, RepeatingTime);
	}
	
	public long getDelay() {
		return DelayBeforeStart;
	}
	
	public long getRepeatingTime() {
		return RepeatingTime;
	}
	
	public boolean getRunningStatus() {
		return running;
	}
	
	public void WhenTaskFinished(BukkitRunnable r) {
		new TaskCreator(new BukkitRunnable() {
			public void run() {
				if(!getRunningStatus()) {
					r.runTask(Main.INSTANCE);
					cancel();
				}
			}
			
		},0L,20L);
	}
	
	public void cancelTask() {
		run.cancel();
		running = false;
	}
}
