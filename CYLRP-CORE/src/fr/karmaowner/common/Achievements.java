package fr.karmaowner.common;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.bukkit.Material;

import java.util.ArrayList;

import fr.karmaowner.companies.*;
import fr.karmaowner.data.CompanyData;
import fr.karmaowner.data.DataAchievements;
import net.md_5.bungee.api.ChatColor;

public enum Achievements{
	WOOD10(ChatColor.RED+"10 bois coup�s",CompanyBucheron.class,"Company",null,(byte)0), 
	WOOD100(ChatColor.RED+"100 bois coup�s",CompanyBucheron.class,"Company",null,(byte)0),
	
	FABRICATION10(ChatColor.RED+"10 objets fabriqu�s",CompanyMenuiserie.class,"Company",null,(byte)0),
	FABRICATION100(ChatColor.RED+"100 objets fabriqu�s",CompanyMenuiserie.class,"Company",null,(byte)0),
	
	MINAGE10(ChatColor.RED+"10 minerais collect�s",CompanyMinage.class,"Company",null,(byte)0),
	MINAGE100(ChatColor.RED+"100 minerais collect�s",CompanyMinage.class,"Company",null,(byte)0),
	
	PECHE10(ChatColor.RED+"10 poissons p�ch�s",CompanyPeche.class,"Company",null,(byte)0),
	PECHE100(ChatColor.RED+"100 poissons p�ch�s",CompanyPeche.class,"Company",null,(byte)0),
	
	PLANTATION10(ChatColor.RED+"10 graines plant�es",CompanyPeche.class,"Company",null,(byte)0),
	PLANTATION100(ChatColor.RED+"100 graines plant�es",CompanyPeche.class,"Company",null,(byte)0),	
	
	TRANSFORMATION10(ChatColor.RED+"10 minerais transform�s",CompanyForgeron.class,"Company",null,(byte)0),
	TRANSFORMATION100(ChatColor.RED+"100 minerais transform�s",CompanyForgeron.class,"Company",null,(byte)0),
	
	STONEAXE(ChatColor.GRAY+"Hache en pierre d�bloqu�",CompanyBucheron.class,"Company",Material.STONE_AXE,(byte)0),
	IRONAXE(ChatColor.WHITE+"Hache en fer d�bloqu�",CompanyBucheron.class,"Company",Material.IRON_AXE,(byte)0),
	GOLDENAXE(ChatColor.GOLD+"Hache en or d�bloqu�",CompanyBucheron.class,"Company",Material.GOLD_AXE,(byte)0),
	DIAMONDAXE(ChatColor.DARK_AQUA+"Hache en pierre d�bloqu�",CompanyBucheron.class,"Company",Material.DIAMOND_AXE,(byte)0),
	SPRUCE(ChatColor.DARK_AQUA+"Bois SPRUCE d�bloqu�",CompanyBucheron.class,"Company",Material.LOG,(byte)1),
	BIRCH(ChatColor.DARK_AQUA+"Bois BIRCH d�bloqu�",CompanyBucheron.class,"Company",Material.LOG,(byte)2),
	JUNGLE(ChatColor.DARK_AQUA+"Bois JUNGLE d�bloqu�",CompanyBucheron.class,"Company",Material.LOG,(byte)3),
	ACACIA(ChatColor.DARK_AQUA+"Bois ACACIA d�bloqu�",CompanyBucheron.class,"Company",Material.LOG_2,(byte)0),
	DARK_OAK(ChatColor.DARK_AQUA+"Bois DARK_OAK d�bloqu�",CompanyBucheron.class,"Company",Material.LOG_2,(byte)1),
	
	TABLE(ChatColor.BLUE+"Tables d�bloqu�s",CompanyMenuiserie.class,"Company",1426,(byte)0),
	;
	
	public static final String MESSAGE = ChatColor.DARK_BLUE+"Succ�s d�bloqu�: ";
	private String name, whichFor;
	private Class<?> classe;
	private Material m;
	private byte data;
	private int id;
	
	Achievements(String name, Class<?> classe, String whichFor, Material m, byte data) {
		this.name = name;
		this.whichFor = whichFor;
		this.m = m;
		this.data =data;
		this.classe = classe;
		
	}
	
	Achievements(String name, Class<?> classe, String whichFor, int id, byte data) {
		this.name = name;
		this.whichFor = whichFor;
		this.id = id;
		this.data =data;
		this.classe = classe;
		
	}
	
	public int getId() {
		return id;
	}
	
	public Class<?> getClasse(){
		return classe;
	}
	
	public static List<Achievements> parseToAchievements(List<String> s) {
		List<Achievements> a = new ArrayList<Achievements>();
		for(Achievements a1 : Achievements.values()) {
			if(s.contains(a1.toString())) {
				a.add(a1);
			}
		}
		return a;
	}
	
	public Material getType() {
		return m;
	}
	
	public Byte getData() {
		return data;
	}
	
	public static void setAchievement(Achievements a, DataAchievements data) {
		if(a != null) {
			data.setAchievements(a);
			if(a.whichFor.equals("Company")) {
				CompanyData cd = (CompanyData)data;
				cd.broadcastCompany(MESSAGE+a.name);
			}
		}
	}
	
	public static ArrayList<Achievements> valuesBySameClass(Class<?> c) {
		ArrayList<Achievements> list = new ArrayList<Achievements>();
		for(Achievements a : values()) {
			if(sameClass(a, c)) {
				list.add(a);
			}
		}
		return list;
	}
	
	public static boolean sameClass(Achievements a1, Achievements a2) {
		return a1.getClasse() == a2.getClasse();
	}
	
	public static boolean sameClass(Achievements a1, Class<?> c) {
		return a1.getClasse() == c;
	}
	
	public static boolean hasAchievement(Achievements a1,DataAchievements data) {
		Iterator<Achievements> it = data.getAchievements().iterator();
		while(it.hasNext()) {
			Achievements a2 = it.next();
			if(a1.equals(a2)) {
				return true;
			}
		}
		return false;
	}
	
}
