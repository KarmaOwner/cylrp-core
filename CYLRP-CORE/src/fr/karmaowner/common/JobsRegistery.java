package fr.karmaowner.common;

import java.util.ArrayList;
import java.util.List;

import fr.karmaowner.jobs.Jobs;

public class JobsRegistery {
	
	public static List<Jobs> registeredJobs = new ArrayList();
	
	/**
	 * 
	 * @param job Class du m�tier � enregistrer h�ritant de la classe Jobs
	 */
	public static void register(Jobs job) {
		registeredJobs.add(job);
		Main.Log("Le m�tier " + job.toString() + " a �t� enregistr�");
	}
	

}
