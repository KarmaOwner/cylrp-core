package fr.karmaowner.common;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import fr.karmaowner.data.PlayerData;
import fr.karmaowner.utils.MessageUtils;
import fr.karmaowner.data.CompanyData;

public class Request {
	
	public static List<Request> requestList = new ArrayList<Request>();
	
	public final static int maxRequest = 20;

	public enum RequestType {
		COMPANY,
		GANG;
	};
	
	private RequestType type;
	
	private String sender;
	private String receiver;
	
	private CompanyData cData;
	private String gangName;
	
	{
		if(requestList.size() >= maxRequest)
			requestList.remove(0);
	}
	
	public Request(RequestType type, String sender, String receiver) {
		this.type = type;
		this.sender = sender;
		this.receiver = receiver;
		if(requestExist(this)) 
			return;
		
		requestList.add(this);
	}
	
	public Request(RequestType type, String sender, String receiver, Object obj) {
		this.type = type;
		this.sender = sender;
		this.receiver = receiver;
		if(requestExist(this)) 
			return;
		
		if(type == RequestType.COMPANY)
		{
			cData = (CompanyData) obj;
			String msg = MessageUtils.getMessageFromConfig("company-invite");
			msg = msg.replaceAll("%sender%", sender);
			msg = msg.replaceAll("%company%", cData.getCompanyName());
			Player pReceiver = Bukkit.getPlayer(receiver);
			if(pReceiver != null)
				pReceiver.sendMessage(msg);
			msg = MessageUtils.getMessageFromConfig("company-invite-sended");
			msg = msg.replaceAll("%receiver%", receiver);
			Player pSender = Bukkit.getPlayer(sender);
			if(pSender != null)
				pSender.sendMessage(msg);
		}
		else if(type == RequestType.GANG)
		{
	
		}
		
		requestList.add(this);
	}
	
	public void destroy() {
		requestList.remove(this);
		Main.Log("Requ�te " + this.toString() + "d�truite");
	}
	
	public static Request findRequest(String value) {
		for(Request request : requestList) {
			if(request.type == RequestType.COMPANY)
				if(request.sender.equalsIgnoreCase(value) || request.cData.getCompanyName().equalsIgnoreCase(value))
					return request;
			else if(request.type == RequestType.GANG)
				if(request.sender.equalsIgnoreCase(value) || request.gangName.equalsIgnoreCase(value))
					return request;
		}
		return null;
	}
	
	public static boolean requestExist(Request request) {
		if(requestList.contains(request))
			return true;
		return false;
	}
	
	public CompanyData getCompanyData() {
		return cData;
	}
	
	
	
	
}
