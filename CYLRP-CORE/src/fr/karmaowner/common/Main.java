package fr.karmaowner.common;

import java.io.File;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.earth2me.essentials.Essentials;
import fr.karmaowner.time.TimeRunnable;
import fr.karmaowner.casino.Casino;
import fr.karmaowner.commands.CommandCasino;
import fr.karmaowner.commands.CommandCompany;
import fr.karmaowner.data.CompanyData;
import fr.karmaowner.data.PlayerData;
import fr.karmaowner.events.CompanyAgricultureEvents;
import fr.karmaowner.events.CompanyBucheronEvents;
import fr.karmaowner.events.CompanyEvents;
import fr.karmaowner.events.CompanyForgeronEvents;
import fr.karmaowner.events.CompanyMenuiserieEvents;
import fr.karmaowner.events.CompanyMetallurgieEvents;
import fr.karmaowner.events.CompanyMinageEvents;
import fr.karmaowner.events.CompanyPecheEvents;
import fr.karmaowner.events.ConnectionEvents;
import fr.karmaowner.events.JackpotEvents;
import fr.karmaowner.events.RouletteEvents;
import fr.karmaowner.events.TictactocEvents;
import fr.karmaowner.jobs.AgentSecret;
import fr.karmaowner.jobs.Armurier;
import fr.karmaowner.jobs.Chimiste;
import fr.karmaowner.jobs.Civile;
import fr.karmaowner.jobs.Cuisinier;
import fr.karmaowner.jobs.Douanier;
import fr.karmaowner.jobs.Garagiste;
import fr.karmaowner.jobs.Gign;
import fr.karmaowner.jobs.Hacker;
import fr.karmaowner.jobs.Maire;
import fr.karmaowner.jobs.Medecin;
import fr.karmaowner.jobs.Militaire;
import fr.karmaowner.jobs.Policier;
import fr.karmaowner.jobs.Pompier;
import fr.karmaowner.jobs.Psychopathe;
import fr.karmaowner.jobs.Rebel;
import fr.karmaowner.jobs.Sdf;
import fr.karmaowner.jobs.Taxi;
import fr.karmaowner.jobs.Terroriste;
import fr.karmaowner.jobs.Voleur;
import fr.karmaowner.utils.ItemUtils;

import com.sk89q.worldguard.bukkit.WGBukkit;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.managers.RegionManager;

public class Main extends JavaPlugin {
	
	public static String prefix = "�7[�bCraftYourLifeRP�7]";
	public static final String version = "1.0";
	public static final String name = "CYLRP-CORE";
	public static final String WORLDNAME = "world";
	
	public static Main INSTANCE;
	public static Essentials essentials;
	public static WorldGuardPlugin WG;
	public static RegionManager Regions;
	
	//Initialisation des donn�es + cr�ation du directory ./plugins/CYLRP-Core/UserData
	@Override
	/**
	 * Initialisation du plugin
	 */
	public void onEnable() {
		INSTANCE = this;
		WG = (WorldGuardPlugin) getServer().getPluginManager().getPlugin("WorldGuard");
		this.saveDefaultConfig();
		registerCommands();
		registerEvents();
		prefix = this.getConfig().getString("prefix").replaceAll("&", "�");
		registerJobs();
		essentials = (Essentials) getServer().getPluginManager().getPlugin("Essentials");
		new TaskCreator(new TimeRunnable(this),0L,20L); // Real time in minecraft time of day
	}
	
	// Lorsque le serveur s'�teint on sauvegarde les donn�es des N joueurs pr�sent dans la hashmap
	@Override
	public void onDisable() {
		PlayerData.SaveDatas();
		CompanyData.SaveDatas();
		System.out.println("disabled " + Main.name);
	}
	
	//On enregistre les commandes
	private void registerCommands() {
		getCommand("entreprise").setExecutor(new CommandCompany());
		getCommand("casino").setExecutor(new CommandCasino());

	}
	
	//On enregistre les �venements
	private void registerEvents() {
		getServer().getPluginManager().registerEvents(new ConnectionEvents(), this);
		getServer().getPluginManager().registerEvents(new JackpotEvents(), this);
		getServer().getPluginManager().registerEvents(new RouletteEvents(), this);
		getServer().getPluginManager().registerEvents(new TictactocEvents(), this);
		getServer().getPluginManager().registerEvents(new CompanyBucheronEvents(), this);
		getServer().getPluginManager().registerEvents(new CompanyEvents(), this);
		getServer().getPluginManager().registerEvents(new CompanyMenuiserieEvents(), this);
		getServer().getPluginManager().registerEvents(new CompanyMinageEvents(), this);
		getServer().getPluginManager().registerEvents(new CompanyForgeronEvents(), this);
		getServer().getPluginManager().registerEvents(new CompanyPecheEvents(), this);
		getServer().getPluginManager().registerEvents(new CompanyMetallurgieEvents(), this);
		getServer().getPluginManager().registerEvents(new CompanyAgricultureEvents(), this);
	}
	
	/**
	 * On enregistre la classe des m�tiers
	 */
	private void registerJobs() {
		JobsRegistery.register(new Civile(ItemUtils.fromId(getConfig().getString("jobs.civile.item-id"), null, getConfig().getString("jobs.civile.display-name").replaceAll("&", "�"), 1), "Civile", getConfig().getStringList("jobs.civile.description"), "", true));
		JobsRegistery.register(new Policier(ItemUtils.fromId(getConfig().getString("jobs.policier.item-id"), null, getConfig().getString("jobs.policier.display-name").replaceAll("&", "�"), 1), "Policier", getConfig().getStringList("jobs.policier.description"), "", false));
		JobsRegistery.register(new Gign(ItemUtils.fromId(getConfig().getString("jobs.gign.item-id"), null, getConfig().getString("jobs.gign.display-name").replaceAll("&", "�"), 1), "Gign", getConfig().getStringList("jobs.gign.description"), "cyrlp-vip", false));
		JobsRegistery.register(new Militaire(ItemUtils.fromId(getConfig().getString("jobs.militaire.item-id"), null, getConfig().getString("jobs.militaire.display-name").replaceAll("&", "�"), 1), "Militaire", getConfig().getStringList("jobs.militaire.description"), "cyrlp-vip+", false)); 
		JobsRegistery.register(new Pompier(ItemUtils.fromId(getConfig().getString("jobs.pompier.item-id"), null, getConfig().getString("jobs.pompier.display-name").replaceAll("&", "�"), 1), "Pompier", getConfig().getStringList("jobs.pompier.description"), "", false));
		JobsRegistery.register(new Voleur(ItemUtils.fromId(getConfig().getString("jobs.voleur.item-id"), null, getConfig().getString("jobs.voleur.display-name").replaceAll("&", "�"), 1), "Voleur", getConfig().getStringList("jobs.militaire.description"), "", false));
		JobsRegistery.register(new Rebel(ItemUtils.fromId(getConfig().getString("jobs.rebel.item-id"), null, getConfig().getString("jobs.rebel.display-name").replaceAll("&", "�"), 1), "Rebel", getConfig().getStringList("jobs.rebel.description"), "", false));
		JobsRegistery.register(new Terroriste(ItemUtils.fromId(getConfig().getString("jobs.terroriste.item-id"), null, getConfig().getString("jobs.terroriste.display-name").replaceAll("&", "�"), 1), "Terroriste", getConfig().getStringList("jobs.terroriste.description"), "cyrlp-vip", false));
		JobsRegistery.register(new Psychopathe(ItemUtils.fromId(getConfig().getString("jobs.psychopathe.item-id"), null, getConfig().getString("jobs.psychopathe.display-name").replaceAll("&", "�"), 1), "Psychopathe", getConfig().getStringList("jobs.psychopathe.description"), "cyrlp-vip+", false));
		JobsRegistery.register(new Sdf(ItemUtils.fromId(getConfig().getString("jobs.sdf.item-id"), null, getConfig().getString("jobs.sdf.display-name").replaceAll("&", "�"), 1), "SDF", getConfig().getStringList("jobs.sdf.description"), "", false));
		JobsRegistery.register(new Cuisinier(ItemUtils.fromId(getConfig().getString("jobs.cuisinier.item-id"), null, getConfig().getString("jobs.cuisinier.display-name").replaceAll("&", "�"), 1), "Cuisinier", getConfig().getStringList("jobs.cuisinier.description"), "", false));
		JobsRegistery.register(new Maire(ItemUtils.fromId(getConfig().getString("jobs.maire.item-id"), null, getConfig().getString("jobs.maire.display-name").replaceAll("&", "�"), 1), "Maire", getConfig().getStringList("jobs.maire.description"), "", false));
		JobsRegistery.register(new Chimiste(ItemUtils.fromId(getConfig().getString("jobs.chimiste.item-id"), null, getConfig().getString("jobs.chimiste.display-name").replaceAll("&", "�"), 1), "Chimiste", getConfig().getStringList("jobs.chimiste.description"), "", false));
		JobsRegistery.register(new Hacker(ItemUtils.fromId(getConfig().getString("jobs.hacker.item-id"), null, getConfig().getString("jobs.hacker.display-name").replaceAll("&", "�"), 1), "Hackeur", getConfig().getStringList("jobs.hacker.description"), "", false));
		JobsRegistery.register(new Medecin(ItemUtils.fromId(getConfig().getString("jobs.medecin.item-id"), null, getConfig().getString("jobs.medecin.display-name").replaceAll("&", "�"), 1), "Medecin", getConfig().getStringList("jobs.medecin.description"), "", false));
		JobsRegistery.register(new Taxi(ItemUtils.fromId(getConfig().getString("jobs.taxi.item-id"), null, getConfig().getString("jobs.taxi.display-name").replaceAll("&", "�"), 1), "Taxi", getConfig().getStringList("jobs.taxi.description"), "", false));
		JobsRegistery.register(new Douanier(ItemUtils.fromId(getConfig().getString("jobs.douanier.item-id"), null, getConfig().getString("jobs.douanier.display-name").replaceAll("&", "�"), 1), "Douanier", getConfig().getStringList("jobs.douanier.description"), "", false));
		JobsRegistery.register(new Garagiste(ItemUtils.fromId(getConfig().getString("jobs.garagiste.item-id"), null, getConfig().getString("jobs.garagiste.display-name").replaceAll("&", "�"), 1), "Garagiste", getConfig().getStringList("jobs.garagiste.description"), "", false));
		JobsRegistery.register(new Armurier(ItemUtils.fromId(getConfig().getString("jobs.armurier.item-id"), null, getConfig().getString("jobs.armurier.display-name").replaceAll("&", "�"), 1), "Armurier", getConfig().getStringList("jobs.armurier.description"), "", false));
		JobsRegistery.register(new AgentSecret(ItemUtils.fromId(getConfig().getString("jobs.agentsecret.item-id"), null, getConfig().getString("jobs.agentsecret.display-name").replaceAll("&", "�"), 1), "AgentSecret", getConfig().getStringList("jobs.agentsecret.description"), "cylrp-vip+", false));
	}
	
	/**
	 * 
	 * @param message message � aficher
	 */
	public static void Log(String message) {
		System.out.println("[" + Main.name + "] " + message);
	}
	
	
	
	
}