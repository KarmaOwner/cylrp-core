package fr.karmaowner.companies;

import org.bukkit.Material;

import fr.karmaowner.common.Achievements;
import fr.karmaowner.companies.Company.XP;
import fr.karmaowner.companies.CompanyMenuiserie.XP_MENUISERIE;
import fr.karmaowner.data.CompanyData;

public class CompanyMinage extends Company{

	public enum XP_MINAGE implements XP{
		STONE(Material.STONE,(byte)0,"Pierre",true),
		GLOWSTONE(Material.GLOWSTONE,(byte)0,"Glowstone",true),
		COAL(Material.COAL_ORE,(byte)0,"Charbon",true),
		REDSTONE(Material.REDSTONE_ORE,(byte)0,"Redstone",true),
		GOLD(Material.GOLD_ORE,(byte)0,"Minerai d'or",true),
		DIAMOND(Material.DIAMOND_ORE,(byte)0,"Minerai de diamant",true),
		IRON(Material.IRON_ORE,(byte)0,"Minerai de fer",true),
		SAND(Material.SAND,(byte)0,"Sable",false),
		LAPIS(Material.LAPIS_ORE,(byte)0,"Minerai de Lapis lazuli",true),
		EMERALD(Material.EMERALD_ORE,(byte)0,"Minerai d'emerald",true),
		OBSIDIAN(Material.OBSIDIAN,(byte)0,"Obsidian",false),
		// outils
		WOODEN_PICKAXE(Material.WOOD_PICKAXE,(byte)0,"Pioche en bois",false),
		DIAMOND_PICKAXE(Material.DIAMOND_PICKAXE,(byte)0,"Pioche en diamant",false),
		GOLD_PICKAXE(Material.GOLD_PICKAXE,(byte)0,"Pioche en or",false),
		IRON_PICKAXE(Material.IRON_PICKAXE,(byte)0,"Pioche en fer",false),
		STONE_PICKAXE(Material.STONE_PICKAXE,(byte)0,"Pioche en pierre",false)
		;
		
		private byte data;
		private int id;
		private Material s;
		private boolean minable;
		private String name;
		
		XP_MINAGE(Material s, byte data, String name, boolean minable) {
			this.data =data;
			this.s = s;
			this.minable = minable;
			this.name= name;
		}

		XP_MINAGE(int id, byte data, String name, boolean minable) {
			this.data =data;
			this.id = id;
			this.minable = minable;
			this.name= name;
		}
		
		public double getXp() {
			// rajouter l'xp
			switch(this) {
				default:
					return 0d;
			}
		}
		
		public String getName() {
			return name;
		}

		public boolean getMinable() {
			return minable;
		}
		
		public int getLevelToUnlock() {
			// rajouter le level
			switch(this) {
				default:
					return 0;
			}
		}
		
		public Byte getData() {
			return this.data;
		}
		
		public int getId() {
			return id;
		}
		
		public Material getType() {
			return this.s;
		}
		
	
	};	
	
	public CompanyMinage(CompanyData data) {
		super(data, "Minerais");
	}

	public XP toXP(Material s, Byte data) {
		for(XP_MINAGE x : XP_MINAGE.values()) {
			if(x.getType() == s && x.getType() != null  && x.getData().equals(data)) {
				return x;
			}
		}
		return null;
	}

	public XP toXP(int id, Byte data) {
		for(XP_MINAGE x : XP_MINAGE.values()) {
			if(x.getId() == id && x.getId() != -1 && x.getData().equals(data)) {
				return x;
			}
		}
		return null;
	}	
	
	
	public boolean isPickaxe(Material item) {
		if(item == Material.DIAMOND_PICKAXE || item == Material.WOOD_PICKAXE 
				|| item == Material.GOLD_PICKAXE || item == Material.IRON_PICKAXE || item == Material.STONE_PICKAXE)
			return true;
		
		return false;
	}
	
	public XP toXP(Material s) {
		for(XP_MINAGE x : XP_MINAGE.values()) {
			if(x.getType() == s) {
				return x;
			}
		}
		return null;
	}
	
	public void setCompanyAchievements() {
		for(Achievements a : Achievements.valuesBySameClass(getClass())) {
			if(!Achievements.hasAchievement(a, data)) {
				if(a.equals(Achievements.MINAGE10)) {
					if(compteur >= 10) Achievements.setAchievement(a, data);
				}else if(a.equals(Achievements.MINAGE100)){
					if(compteur >= 100) Achievements.setAchievement(a, data);
				}else {
					super.setCompanyAchievements(a);
				}
			}
		}
	}

}
