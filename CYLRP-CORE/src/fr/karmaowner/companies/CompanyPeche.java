package fr.karmaowner.companies;

import org.bukkit.Material;

import fr.karmaowner.common.Achievements;
import fr.karmaowner.companies.Company.XP;
import fr.karmaowner.companies.CompanyMinage.XP_MINAGE;
import fr.karmaowner.data.CompanyData;

public class CompanyPeche extends Company{
	public enum XP_PECHE implements XP{
		RAW(Material.RAW_FISH,(byte)0,"Poisson cru"),
		CLOWN(Material.RAW_FISH,(byte)2,"Poisson clown"),
		PUFFER(Material.RAW_FISH,(byte)3,"Poisson globe")
		;
		
		private byte data;
		private int id;
		private Material s;
		private String name;
		
		XP_PECHE(Material s, byte data, String name) {
			this.data =data;
			this.s = s;
			this.name= name;
		}

		XP_PECHE(int id, byte data, String name) {
			this.data =data;
			this.id = id;
			this.name= name;
		}
		
		public double getXp() {
			// rajouter l'xp
			switch(this) {
				default:
					return 0d;
			}
		}
		
		public String getName() {
			return name;
		}
		
		public int getLevelToUnlock() {
			// rajouter le level
			switch(this) {
				case RAW:
					return 2;
				default:
					return 0;
			}
		}
		
		public Byte getData() {
			return this.data;
		}
		
		public int getId() {
			return id;
		}
		
		public Material getType() {
			return this.s;
		}
		
	
	};	
	
	public CompanyPeche(CompanyData data) {
		super(data, "Poissons");
	}

	public XP toXP(Material s, Byte data) {
		for(XP_PECHE x : XP_PECHE.values()) {
			if(x.getType() == s && x.getType() != null  && x.getData().equals(data)) {
				return x;
			}
		}
		return null;
	}

	public XP toXP(int id, Byte data) {
		for(XP_PECHE x : XP_PECHE.values()) {
			if(x.getId() == id && x.getId() != -1 && x.getData().equals(data)) {
				return x;
			}
		}
		return null;
	}	
	
	public XP toXP(Material s) {
		for(XP_PECHE x : XP_PECHE.values()) {
			if(x.getType() == s) {
				return x;
			}
		}
		return null;
	}
	
	public void setCompanyAchievements() {
		for(Achievements a : Achievements.valuesBySameClass(getClass())) {
			if(!Achievements.hasAchievement(a, data)) {
				if(a.equals(Achievements.PECHE10)) {
					if(compteur >= 10) Achievements.setAchievement(a, data);
				}else if(a.equals(Achievements.PECHE100)){
					if(compteur >= 100) Achievements.setAchievement(a, data);
				}else {
					super.setCompanyAchievements(a);
				}
			}
		}
	}

}
