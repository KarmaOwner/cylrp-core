package fr.karmaowner.companies;

import org.bukkit.Material;

import fr.karmaowner.common.Achievements;
import fr.karmaowner.companies.Company.XP;
import fr.karmaowner.data.CompanyData;

public class CompanyAgriculture extends Company{

	public enum XP_AGRICULTURE implements XP{
		//Fruits et l�gumes � ramasser
		/*TOMATO(5520,(byte)0,"Tomate",0,0),
		STRAWBERRY(5519,(byte)0,"Fraise",0,0),
		ORANGE(5667,(byte)0,"Orange",0,0),
		PEAR(5668,(byte)0,"Poire",0,0),
		BANANA(5700,(byte)0,"Banane",0,0),
		CHERRY(5701,(byte)0,"Cerise",0,0),
		COCONUT(5702,(byte)0,"Noix de coco",0,0),
		KIWI(5703,(byte)0,"Kiwi",0,0),
		WALNUT(5445,(byte)0,"Noix",0,0),
		MELON(Material.MELON_STEM,(byte)0,"Melon",0,0),
		PUMPKIN(Material.PUMPKIN,(byte)0,"Citrouille",0,0),
		WHEAT(Material.WHEAT,(byte)0,"Bl�",0,0),*/
		
		
		//Graines
		CROPS(Material.CROPS,(byte)0,"graine de bl�",0,0),
		MELONSTEM(Material.MELON_STEM,(byte)0,"graine de past�que",0,0),
		PUMPKINSTEM(Material.PUMPKIN_STEM,(byte)0,"graine de citrouille",0,0),
		ORANGESAPLINGS(845,(byte)0,"Oranger",0,0),
		PEARSAPLINGS(846,(byte)0,"Poirier",0,0),
		KIWISAPLINGS(855,(byte)0,"Arbre de Kiwis",0,0),
		WALNUTSAPLINGS(868,(byte)0,"Noyer",0,0),
		CHERRYSAPLINGS(857,(byte)0,"Cerisier",0,0),
		BANANASAPLINGS(861,(byte)0,"Bananier",0,0),
		COCONUTSAPLINGS(845,(byte)0,"Cocotier",0,0),
		TOMATOSEEDS(5603,(byte)0,"Graine de tomate",0,0),
		STRAWBERRYSEEDS(5604,(byte)0,"Graine de cerise",0,0),
		
		
		//Outils pour semer la terre
		WOODHOE(Material.WOOD_HOE,(byte)0,"Houe en bois",0,0),
		STONEHOE(Material.STONE_HOE,(byte)0,"Houe en pierre",0,0),
		IRONHOE(Material.IRON_HOE,(byte)0,"Houe en fer",0,0),
		GOLDHOE(Material.GOLD_HOE,(byte)0,"Houe en or",0,0),
		DIAMONDHOE(Material.DIAMOND_HOE,(byte)0,"Houe en diamant",0,0),
		;
		
		private byte data;
		private int id, level;
		private Material s;
		private String name;
		private double xp;
		
		XP_AGRICULTURE(Material s, byte data, String name, double xp, int level) {
			this.data =data;
			this.s = s;
			this.name= name;
			this.xp = xp;
			this.level = level;
		}

		XP_AGRICULTURE(int id, byte data, String name, double xp, int level) {
			this.data =data;
			this.id = id;
			this.name= name;
			this.xp = xp;
			this.level = level;
		}
		
		public String getName() {
			return name;
		}
		
		public double getXp() {
			return xp;
		}

		
		public int getLevelToUnlock() {
			return level;
		}
		
		public Byte getData() {
			return this.data;
		}
		
		public int getId() {
			return id;
		}
		
		public Material getType() {
			return this.s;
		}
		
	
	};
	
	public CompanyAgriculture(CompanyData data) {
		super(data, "Plantations");
	}
	
	public boolean isHoe(Material m) {
		return m == Material.GOLD_HOE || m == Material.WOOD_HOE || m == Material.DIAMOND_HOE 
				|| m == Material.IRON_HOE || m == Material.STONE_HOE;
	}

	public XP toXP(Material s, Byte data) {
		if(s == null) return null;
		if(data == null) return null;
		for(XP_AGRICULTURE x : XP_AGRICULTURE.values()) {
			if(s == x.getType() && data == x.getData()) {
				return x;
			}
		}
		return null;
	}
	
	public XP toXP(int id, Byte data) {
		if(id == 0) return null;
		if(data == null) return null;
		for(XP_AGRICULTURE x : XP_AGRICULTURE.values()) {
			if(id == x.getId() && data == x.getData()) {
				return x;
			}
		}
		return null;
	}
	
	public XP toXP(Material s) {
		for(XP_AGRICULTURE x : XP_AGRICULTURE.values()) {
			if(x.getType() == s) {
				return x;
			}
		}
		return null;
	}
	
	public void setCompanyAchievements() {
		for(Achievements a : Achievements.valuesBySameClass(getClass())) {
			if(!Achievements.hasAchievement(a, data)) {
				if(a.equals(Achievements.PLANTATION10)) {
					if(compteur >= 10) Achievements.setAchievement(a, data);
				}else if(a.equals(Achievements.PLANTATION100 )){
					if(compteur >= 100) Achievements.setAchievement(a, data);
				}else {
					super.setCompanyAchievements(a);
				}
			}
		}
	}
	
}
