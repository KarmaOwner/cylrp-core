package fr.karmaowner.companies;

import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;

import fr.karmaowner.common.Achievements;
import fr.karmaowner.data.CompanyData;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.event.block.BlockBreakEvent;
import fr.karmaowner.utils.ItemUtils;

public class CompanyBucheron extends Company{
	
	public enum XP_BUCHERON implements XP{
		WOOD0(Material.LOG,(byte)0,"Bois de ch�ne"), 
		WOOD1(Material.LOG,(byte)1,"Bois d'�picea"), 
		WOOD2(Material.LOG,(byte)2,"Bois de bouleau"), 
		WOOD3(Material.LOG,(byte)3,"Bois de jungle"), 
		WOOD4(Material.LOG_2,(byte)0,"Bois d'acacia"), 
		WOOD5(Material.LOG_2,(byte)1,"Bois de ch�ne fonc�"),
		
		// outils
		STONE_AXE(Material.STONE_AXE,(byte)0,"Hache en pierre"), 
		WOOD_AXE(Material.WOOD_AXE,(byte)0,"Hache en bois"), 
		IRON_AXE(Material.IRON_AXE,(byte)0,"Hache en fer"), 
		GOLDEN_AXE(Material.GOLD_AXE,(byte)0,"Hache en or"),
		DIAMOND_AXE(Material.DIAMOND_AXE,(byte)0,"Hache en diamant")
		;
		
		private byte data;
		private Material s;
		private int id;
		private String name;
		
		XP_BUCHERON(Material s, byte data, String name) {
			this.data =data;
			this.s = s;
			this.name= name;
		}
		XP_BUCHERON(int id, byte data, String name) {
			this.data =data;
			this.id = id;
			this.name= name;
		}
		
		public String getName() {
			return name;
		}
		
		/**
		 * XP_BUCHERON a remporté en cassant un bois particulier
		 * @return l'XP_BUCHERON
		 */
		public double getXp() {
			switch(this) {
			case WOOD0:
				return 50d;
			case WOOD1:
				return 100d;
			case WOOD2:
				return 75d;
			case WOOD3:
				return 115d;
			case WOOD4:
				return 130d;
			case WOOD5:
				return 145d;
			default:
				return 0d;
			}
		}
		
		public Byte getData() {
			return this.data;
		}
		
		public Material getType() {
			return this.s;
		}
		
		/**
		 * Niveau requis pour débloquer des objets
		 * @return le niveau
		 */
		public int getLevelToUnlock() {
			switch(this) {
			case WOOD2:
				return 15;
			case WOOD1:
				return 30;
			case WOOD3:
				return 50;
			case WOOD4:
				return 85;
			case WOOD5:
				return 125;
			case STONE_AXE:
				return 20;
			case IRON_AXE:
				return 60;
			case GOLDEN_AXE:
				return 100;
			case DIAMOND_AXE:
				return 150;
			default:
				return 0;
			}
		}

		public int getId() {
			return id;
		}
	}
	
	public CompanyBucheron(CompanyData data) {
		super(data,"Bois");
	}
	
	public XP toXP(Material s, Byte data) {
		for(XP_BUCHERON x : XP_BUCHERON.values()) {
			if( x.getType() == s && x.getType() != null && x.getData().equals(data)) {
				return x;
			}
		}
		return null;
	}
	
	public XP toXP(int id, Byte data) {
		for(XP_BUCHERON x : XP_BUCHERON.values()) {
			if( x.getId() == id && x.getId() != -1 && x.getData().equals(data)) {
				return x;
			}
		}
		return null;
	}
	
	public XP toXP(Material s) {
		for(XP_BUCHERON x : XP_BUCHERON.values()) {
			if(x.getType() == s) {
				return x;
			}
		}
		return null;
	}
	
	public boolean isAxe(Material item) {
		return item == Material.DIAMOND_AXE || item == Material.GOLD_AXE ||
				item == Material.STONE_AXE || item == Material.IRON_AXE 
				|| item == Material.WOOD_AXE;
		
	}

	public void setCompanyAchievements() {
		for(Achievements a : Achievements.valuesBySameClass(getClass())) {
			if(!Achievements.hasAchievement(a, data)) {
				if(a.equals(Achievements.WOOD10)) {
					if(compteur >= 10) Achievements.setAchievement(a, data);
				}else if(a.equals(Achievements.WOOD100)){
					if(compteur >= 100) Achievements.setAchievement(a, data);
				}else {
					super.setCompanyAchievements(a);
				}
			}
		}
		
	}
}
