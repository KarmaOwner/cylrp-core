package fr.karmaowner.companies;

import org.bukkit.Bukkit;
import org.bukkit.Material;

import fr.karmaowner.common.Achievements;
import fr.karmaowner.companies.Company.XP;
import fr.karmaowner.companies.CompanyBucheron.XP_BUCHERON;
import fr.karmaowner.data.CompanyData;

public class CompanyMenuiserie extends Company{

	public enum XP_MENUISERIE implements XP{
		PLANK_OAK(Material.WOOD,(byte)0,"Planche en ch�ne"),PLANK_SPRUCE(Material.WOOD,(byte)1,"planche en �picia"),
		PLANK_BIRCH(Material.WOOD,(byte)2,"planche en bouleau"),
		PLANK_JUNGLE(Material.WOOD,(byte)3,"planche en bois de jungle"),PLANK_ACACIA(Material.WOOD,(byte)4,"planche en acacia"),
		PLANK_DARK_OAK(Material.WOOD,(byte)5,"planche en ch�ne fonc�"),
		CHAISE0(1334,(byte)0,"Chaise"),CHAISE1(1334,(byte)1,"Chaise"),
		CHAISE2(1334,(byte)2,"Chaise"),CHAISE3(1334,(byte)3,"Chaise"),CHAISE4(1334,(byte)4,"Chaise"),CHAISE5(1334,(byte)5,"Chaise"),
		BUREAU0(1338,(byte)0,"Bureau"),BUREAU1(1338,(byte)1,"Bureau"),BUREAU2(1338,(byte)2,"Bureau"),
		BUREAU3(1338,(byte)3,"Bureau"),BUREAU4(1338,(byte)4,"Bureau"),BUREAU5(1338,(byte)5,"Bureau"),
		HORLOGE0(1344,(byte)0,"Horloge"),HORLOGE1(1344,(byte)1,"Horloge"),HORLOGE2(1344,(byte)2,"Horloge"),HORLOGE3(1344,(byte)3,"Horloge"),
		HORLOGE4(1344,(byte)4,"Horloge"),HORLOGE5(1344,(byte)5,"Horloge"),
		TABLE0(1426,(byte)0,"Table"),TABLE1(1426,(byte)1,"Table"),TABLE2(1426,(byte)2,"Table"),TABLE3(1426,(byte)3,"Table"),
		TABLE4(1426,(byte)4,"Table"),TABLE5(1426,(byte)5,"Table"),
		ETAGERE0(1430,(byte)0,"�tag�re"),ETAGERE1(1430,(byte)1,"�tag�re"),ETAGERE2(1430,(byte)2,"�tag�re"),ETAGERE3(1430,(byte)3,"�tag�re"),
		ETAGERE4(1430,(byte)4,"�tag�re"),ETAGERE5(1430,(byte)5,"�tag�re"),
		BIBLIOTHEQUE0(1384,(byte)0,"Biblioth�que"),BIBLIOTHEQUE1(1384,(byte)1,"Biblioth�que"),BIBLIOTHEQUE2(1384,(byte)2,"Biblioth�que"),
		BIBLIOTHEQUE3(1384,(byte)3,"Biblioth�que"),BIBLIOTHEQUE4(1384,(byte)4,"Biblioth�que"),BIBLIOTHEQUE5(1384,(byte)5,"Biblioth�que"),
		PANNEAU0(1367,(byte)0,"Panneau"),PANNEAU1(1367,(byte)1,"Panneau"),PANNEAU2(1367,(byte)2,"Panneau"),PANNEAU3(1367,(byte)3,"Panneau"),
		PANNEAU4(1367,(byte)4,"Panneau"),PANNEAU5(1367,(byte)5,"Panneau"),
		PORTE0(1373,(byte)0,"Porte"),PORTE1(1373,(byte)1,"Porte"),PORTE2(1373,(byte)2,"Porte"),PORTE3(1373,(byte)3,"Porte"),
		PORTE4(1373,(byte)4,"Porte"),PORTE5(1373,(byte)5,"Porte")
		// pickaxe 
		// axe
		// sign
		// hoe
		// paiting
		// bed
		// item frame
		// fancy sign
		// canne � p�che
		;
		
		private byte data;
		private int id;
		private Material s;
		private String name;
		
		XP_MENUISERIE(Material s, byte data, String name) {
			this.data =data;
			this.s = s;
			this.name= name;
		}

		XP_MENUISERIE(int id, byte data, String name) {
			this.data =data;
			this.id = id;
			this.name= name;
		}
		
		public String getName() {
			return name;
		}
		
		public double getXp() {
			if(this.getType() == Material.WOOD) {
				return 50 / 12d;
			}else if(this.getId() == 1426) {
				return 25d;
			}else if(this.getId() == 1338) {
				return 50d;
			}else if(this.getId() == 1430) {
				return 75d;
			}else if(this.getId() == 1344) {
				return 100d;
			}else if(this.getId() == 1384) {
				return 125d;
			}else if(this.getId() == 1367) {
				return 150d;
			}else if(this.getId() == 1373) {
				return 175d;
			}else {
				return 0d;
			}
		}

		
		public int getLevelToUnlock() {
			if(this.getId() == 1426) {
				return 1;
			}else if(this.getType() == Material.WOOD) {
				return 15;
			}else if(this.getId() == 1338) {
				return 30;
			}else if(this.getId() == 1430) {
				return 50;
			}else if(this.getId() == 1344) {
				return 75;
			}else if(this.getId() == 1384) {
				return 100;
			}else if(this.getId() == 1367) {
				return 130;
			}else if(this.getId() == 1373) {
				return 200;
			}else {
				return 0;
			}
		}
		
		public Byte getData() {
			return this.data;
		}
		
		public int getId() {
			return id;
		}
		
		public Material getType() {
			return this.s;
		}
		
	
	};
	
	public CompanyMenuiserie(CompanyData data) {
		super(data, "Objets");
	}

	public XP toXP(Material s, Byte data) {
		if(s == null) return null;
		if(data == null) return null;
		for(XP_MENUISERIE x : XP_MENUISERIE.values()) {
			if(s == x.getType() && data == x.getData()) {
				return x;
			}
		}
		return null;
	}
	
	public XP toXP(int id, Byte data) {
		if(id == 0) return null;
		if(data == null) return null;
		for(XP_MENUISERIE x : XP_MENUISERIE.values()) {
			if(id == x.getId() && data == x.getData()) {
				return x;
			}
		}
		return null;
	}
	
	public XP toXP(Material s) {
		for(XP_MENUISERIE x : XP_MENUISERIE.values()) {
			if(x.getType() == s) {
				return x;
			}
		}
		return null;
	}
	
	public void setCompanyAchievements() {
		for(Achievements a : Achievements.valuesBySameClass(getClass())) {
			if(!Achievements.hasAchievement(a, data)) {
				if(a.equals(Achievements.FABRICATION10)) {
					if(compteur >= 10) Achievements.setAchievement(a, data);
				}else if(a.equals(Achievements.FABRICATION100 )){
					if(compteur >= 100) Achievements.setAchievement(a, data);
				}else {
					super.setCompanyAchievements(a);
				}
			}
		}
	}

}
