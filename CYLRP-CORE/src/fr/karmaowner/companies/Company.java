package fr.karmaowner.companies;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import com.sk89q.worldguard.LocalPlayer;
import com.sk89q.worldguard.domains.Association;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

import fr.karmaowner.common.Achievements;
import fr.karmaowner.common.Main;
import fr.karmaowner.companies.Company.XP;
import fr.karmaowner.companies.CompanyBucheron.XP_BUCHERON;
import fr.karmaowner.data.CompanyData;
import net.md_5.bungee.api.ChatColor;

public abstract class Company implements Listener{
	
	public enum TYPE{
		BUCHERON, MENUISERIE, MINAGE, FORGERON, PECHE, METALLURGIE, AGRICULTURE;
		
		public Company getCompany(CompanyData data) {
			switch(this) {
			case BUCHERON:
				return new CompanyBucheron(data);
			case MENUISERIE:
				return new CompanyMenuiserie(data);
			case MINAGE:
				return new CompanyMinage(data);
			case FORGERON:
				return new CompanyForgeron(data);
			case PECHE:
				return new CompanyPeche(data);
			case METALLURGIE:
				return new CompanyMetallurgie(data);
			case AGRICULTURE:
				return new CompanyAgriculture(data);
			default:
				return null;
			}
		}
		
	}
	
	// Using interface for Enumeration in child class
	public interface XP{
		double getXp();
		int getLevelToUnlock();
		String getName();
	}
	
	public Company(CompanyData data, String CompteurName) {
		this.data = data;
		this.CompteurName = CompteurName;
		this.isThatRegion = false;
		loadData();
	}
	
	abstract XP toXP(Material s, Byte data); // Convert Material and Byte to XP
	abstract XP toXP(int id, Byte data); // Convert id and Byte to XP
	abstract XP toXP(Material s); // convert Material to XP
	
	protected CompanyData data = null;
	private String CompteurName; // Unit� de repr�sentation de l'activit� de l'entreprise Ex: Du bois
	protected int compteur; // En repr�nant l'exemple du dessus, �a serait le nombre de bois
	public boolean isThatRegion; // Si le joueur est membre d'une r�gion ( on va autoriser de poser et casser des blocs)
	
	public static Company getInstanceByCategorie(String categorie,CompanyData data) {
		for(TYPE t : TYPE.values()) {
			if(categorie.equalsIgnoreCase(t.toString())) {
				return t.getCompany(data);
			}
		}
		return null;
	}
	
	public boolean isItemUnlocked(XP item) {
		if(item != null)
		return data.getLevelReached() >= item.getLevelToUnlock();
		else
		return false;
	}
	
	public int getCompteur() {
		return compteur;
	}
	
	public void setCompteur(int compteur) {
		this.compteur = compteur;
	}
	
	public String getCompteurName() {
		return CompteurName;
	}
	
	public void setCompanyAchievements(Achievements a) {
		if(!Achievements.hasAchievement(a, data)) {
			XP x = toXP(a.getType(),a.getData());
			x = ( x == null ? toXP(a.getId(),a.getData()) : x);
			if(x != null)
				if(isItemUnlocked(x))
					Achievements.setAchievement(a, data);
		}
	}
	
	public void setXp(Player p, XP item) {
		data.setXp(item.getXp());
		p.sendMessage("�cVous venez de remporter +�4"+item.getXp()+" �cxp");
		data.getCompany().setCompteur(data.getCompany().getCompteur()+1);
	}
	
	public void locked_Message(Player p, XP item) {
		p.sendMessage(ChatColor.DARK_RED+item.getName()+" non d�bloqu�: disponible au niveau "+ChatColor.RED+""+item.getLevelToUnlock());
	}
	
	public void loadData() {
		if(data.getFileConfiguration() != null && !data.isCreation)
			compteur = data.getFileConfiguration().getInt(CompteurName);
	}
	
	public void saveData() {
		data.getFileConfiguration().set(CompteurName, compteur);
	}
}
