package fr.karmaowner.companies;

import org.bukkit.Material;

import fr.karmaowner.common.Achievements;
import fr.karmaowner.data.CompanyData;

public class CompanyMetallurgie extends Company{

	public enum XP_METALLURGIE implements XP{
		IRONDOOR(Material.IRON_DOOR,(byte)0,"porte en fer",0,0),
		REINFORCEDIRONDOOR(4497,(byte)0,"porte en fer blind�e",0,0),
		GOLDBLOCK(Material.GOLD_BLOCK,(byte)0,"bloc en or",0,0),
		IRONBLOCK(Material.IRON_BLOCK,(byte)0,"bloc en fer",0,0),
		DIAMONDBLOCK(Material.DIAMOND_BLOCK,(byte)0,"bloc en diamand",0,0),
		ALUMINIUM(4435,(byte)0,"Aluminium",0,0),
		STEELPLATE(4443,(byte)0,"Plaque D'acier",0,0),
		DURASTEEL(4431,(byte)0,"Acier Dur",0,0),
		TITANINGOT(4436,(byte)0,"Lingot titan",0,0),
		CARWHEEL(4487,(byte)0,"Roue de voiture",0,0),
		APCWHEEL(4485,(byte)0,"Roue d'APC",0,0),
		TRUCKWHEEL(4491,(byte)0,"Roue de truck",0,0),
		CARCHASSIS(4398,(byte)0,"Ch�ssis de voiture",0,0),
		TRUCKCHASSIS(4402,(byte)0,"Ch�ssis de truck",0,0),
		VANCHASSIS(4403,(byte)0,"Ch�ssis de van",0,0),
		STEELARMORPLATE(4426,(byte)0,"Plaque de blindage en acier",0,0),
		TITANARMORPLATE(4427,(byte)0,"Plaque de blindage en titan",0,0),
		OILBARREL(4428,(byte)0,"Barril de p�trole",0,0),
		GUMIBLOCK(4432,(byte)0,"Caoutchouc",0,0)
		;
		
		private byte data;
		private int id, level;
		private Material s;
		private String name;
		private double xp;
		
		XP_METALLURGIE(Material s, byte data, String name, double xp, int level) {
			this.data =data;
			this.s = s;
			this.name= name;
			this.xp = xp;
			this.level = level;
		}

		XP_METALLURGIE(int id, byte data, String name, double xp, int level) {
			this.data =data;
			this.id = id;
			this.name= name;
			this.xp = xp;
			this.level = level;
		}
		
		public String getName() {
			return name;
		}
		
		public double getXp() {
			return xp;
		}

		
		public int getLevelToUnlock() {
			return level;
		}
		
		public Byte getData() {
			return this.data;
		}
		
		public int getId() {
			return id;
		}
		
		public Material getType() {
			return this.s;
		}
		
	
	};
	
	public CompanyMetallurgie(CompanyData data) {
		super(data, "Objets");
	}

	public XP toXP(Material s, Byte data) {
		if(s == null) return null;
		if(data == null) return null;
		for(XP_METALLURGIE x : XP_METALLURGIE.values()) {
			if(s == x.getType() && data == x.getData()) {
				return x;
			}
		}
		return null;
	}
	
	public XP toXP(int id, Byte data) {
		if(id == 0) return null;
		if(data == null) return null;
		for(XP_METALLURGIE x : XP_METALLURGIE.values()) {
			if(id == x.getId() && data == x.getData()) {
				return x;
			}
		}
		return null;
	}
	
	public XP toXP(Material s) {
		for(XP_METALLURGIE x : XP_METALLURGIE.values()) {
			if(x.getType() == s) {
				return x;
			}
		}
		return null;
	}
	
	public void setCompanyAchievements() {
		for(Achievements a : Achievements.valuesBySameClass(getClass())) {
			if(!Achievements.hasAchievement(a, data)) {
				if(a.equals(Achievements.FABRICATION10)) {
					if(compteur >= 10) Achievements.setAchievement(a, data);
				}else if(a.equals(Achievements.FABRICATION100 )){
					if(compteur >= 100) Achievements.setAchievement(a, data);
				}else {
					super.setCompanyAchievements(a);
				}
			}
		}
	}
}
