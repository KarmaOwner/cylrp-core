package fr.karmaowner.companies;

import java.util.ArrayList;

import org.bukkit.Material;

import fr.karmaowner.common.Achievements;
import fr.karmaowner.companies.CompanyMinage.XP_MINAGE;
import fr.karmaowner.data.CompanyData;

public class CompanyForgeron extends Company{
	public enum XP_FORGERON implements XP{		
		DIAMOND(Material.DIAMOND,(byte)0,"Diamand"),
		EMERALD(Material.EMERALD,(byte)0,"Emeraude"),
		COAL(Material.COAL,(byte)0,"Charbon"),
		LAPIS(Material.INK_SACK,(byte)4,"Lapis lazuli"),
		GOLD(Material.GOLD_INGOT,(byte)0,"Or"),
		IRON(Material.IRON_INGOT,(byte)0,"Fer"),
		REDSTONE(Material.REDSTONE,(byte)0,"Redstone"),
		STEEL(4435,(byte)0,"Acier")
		;
		
		private byte data;
		private int id;
		private Material s;
		private String name;
		
		XP_FORGERON(Material s, byte data, String name) {
			this.data =data;
			this.s = s;
			this.name= name;
		}
		
		XP_FORGERON(int id, byte data, String name) {
			this.data =data;
			this.id = id;
			this.name= name;
		}
		
		public String getName() {
			return name;
		}
		
		public double getXp() {
				return 0d;
		}

		
		public int getLevelToUnlock() {
			switch(this) {
				case IRON:
					return 2;
				default:
					return 0;
			}
		}
		
		public Byte getData() {
			return this.data;
		}
		
		public int getId() {
			return id;
		}
		
		public Material getType() {
			return this.s;
		}
		
	
	};
	
	public CompanyForgeron(CompanyData data) {
		super(data, "Transformations");
	}

	public XP toXP(Material s, Byte data) {
		if(s == null) return null;
		if(data == null) return null;
		for(XP_FORGERON x : XP_FORGERON.values()) {
			if(s == x.getType() && data == x.getData()) {
				return x;
			}
		}
		return null;
	}
	
	public XP toXP(int id, Byte data) {
		if(id == 0) return null;
		if(data == null) return null;
		for(XP_FORGERON x : XP_FORGERON.values()) {
			if(id == x.getId() && data == x.getData()) {
				return x;
			}
		}
		return null;
	}
	
	public XP toXP(Material s) {
		for(XP_FORGERON x : XP_FORGERON.values()) {
			if(x.getType() == s) {
				return x;
			}
		}
		return null;
	}
	
	public void setCompanyAchievements() {
		for(Achievements a : Achievements.valuesBySameClass(getClass())) {
			if(!Achievements.hasAchievement(a, data)) {
				if(a.equals(Achievements.TRANSFORMATION10)) {
					if(compteur >= 10) Achievements.setAchievement(a, data);
				}else if(a.equals(Achievements.TRANSFORMATION100 )){
					if(compteur >= 100) Achievements.setAchievement(a, data);
				}else {
					super.setCompanyAchievements(a);
				}
			}
		}
	}

	
	
}
