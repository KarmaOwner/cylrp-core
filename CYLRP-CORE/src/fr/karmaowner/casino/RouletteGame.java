package fr.karmaowner.casino;

import java.util.ArrayList;
import java.util.HashMap;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkMeta;

import fr.karmaowner.common.TaskCreator;
import net.md_5.bungee.api.ChatColor;
import net.minecraft.server.v1_7_R4.EntityFireworks;

public class RouletteGame extends Casino{

	public Player p;
	public HashMap<Player,Double> multiply;
	private HashMap<Player,Double> mise;
	private TaskCreator ChoosingMultiply, Game;
	private int TimeMultiply;
	private int TimeLeft, Time;
	
	public final double[] itemRatio = {
			1.5d, 2d, 2.5d
	};
	
	private final int[] idMaterial = {
			14, 3, 4
	};
	
	private final Color[] FIREWORKCOLOR = {
			Color.RED, Color.BLUE, Color.YELLOW
	};
	
	public RouletteGame(Player p) {
		super(10, 5, false,true);
		b = new Bet(this,0);
		this.p = p;
		multiply = new HashMap<Player,Double>();
		TimeMultiply = 5;
		TimeLeft = (int)(Math.random()*(32-30+1)+30);
		Bukkit.broadcastMessage(""+TimeLeft);
		Time = TimeLeft;
		inv = Bukkit.createInventory(null, 27, ChatColor.BOLD+""+ChatColor.BLUE+"Roulette");
		addItemInventory(Material.STAINED_GLASS_PANE, 14, 11, ChatColor.RED+"x1.5");
		addItemInventory(Material.STAINED_GLASS_PANE, 3, 13, ChatColor.BLUE+"x2");
		addItemInventory(Material.STAINED_GLASS_PANE, 4, 15, ChatColor.YELLOW+"x2.5");
	}
	
	public void WaitPlayerMultiply() {
		ChoosingMultiply = new TaskCreator(new BukkitRunnable() {
			public void run() {
				if(!b.taskBet.getRunningStatus()) {
					openInventory();
					if(--TimeMultiply <= 0) {
						closeInventory();
						for(Player p : listPlayers) {
							if(multiply.get(p) == null) {
								int random = (int)(Math.random()*(itemRatio.length));
								multiply.put(p, itemRatio[random]);
								p.sendMessage("Nous avons choisi � votre place le multiplicateur par x"+multiply.get(p));
							}
						}
						ChoosingMultiply.cancelTask();
						loadGame();
					}else {
						NotificatePlayers("Temps restant avant le d�but du tirage: "+TimeMultiply+"s");
					}
				}
			}
		},0L,20L*2);
	}
	
	public void Winner(Byte id) {
		int i;
		for(i = 0; i < idMaterial.length;i++) {
			if(idMaterial[i] == id) {
				break;
			}
		}
		
		
		for(Player p : listPlayers) {
			if(multiply.get(p) == itemRatio[i]) {
				Firework f = (Firework)p.getWorld().spawn(p.getLocation(), Firework.class);
				FireworkMeta effect = f.getFireworkMeta();
				effect.setPower(1);
				effect.addEffect(FireworkEffect.builder()
						.withColor(FIREWORKCOLOR[i])
						.flicker(true)
						.build());
				f.setFireworkMeta(effect);
				new TaskCreator(new BukkitRunnable() {
					public void run() {
						f.detonate();
						cancel();
					}
				}, 10L, 0L);
				// les gagnants
				double total = b.bet.get(p)*multiply.get(p);
				p.sendMessage("Vous venez de remporter la cagnotte");
				p.sendMessage("Votre cagnotte s'�l�ve � "+total);
			}else {
				p.sendMessage("Vous avez perdu l'int�gralit� de votre mise");
			}
					
		}
	}
	
	public void loadGame() {
		inv = Bukkit.createInventory(null, InventoryType.HOPPER, ChatColor.BOLD+""+ChatColor.BLUE+"Roulette");
		for(int i = 0; i < 5; i++) {
			addItemInventory(Material.STAINED_GLASS_PANE, idMaterial[i%3], i, "");
		}
		Game = new TaskCreator(new BukkitRunnable() {
			public void run() {
				openInventory();
				if(--TimeLeft <= 0) {
					byte id = inv.getItem(2).getData().getData();
					Winner(id);
					Game.cancelTask();
					NotificatePlayers("La couleur qui remporte la partie est le "+(id == 14 ? ChatColor.RED+"Rouge" : 
						(id == 3 ? ChatColor.BLUE+"Bleu" : ChatColor.YELLOW+"Jaune")) );
					endGame();
				}else {
					for(int i = 0; i < 5; i++) {
						addItemInventory(Material.STAINED_GLASS_PANE, idMaterial[((Time - TimeLeft)+i)%3], i, "");
					}
				}
			}
		},0L,10L);
	}

	public void startGame() {
		if(s == State.WAITING) {
			s = State.STARTED;
			b.startMise();
			b.taskBet.WhenTaskFinished(new BukkitRunnable() {
				public void run() {
					WaitPlayerMultiply();
				}
			});
		}
	}
	
	public String toString() {
		return ChatColor.GREEN+"Roulette"+super.toString();
	}
}
