package fr.karmaowner.casino;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.HashMap;

import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import fr.karmaowner.casino.Casino.State;
import fr.karmaowner.common.Main;
import fr.karmaowner.common.TaskCreator;
import fr.karmaowner.data.PlayerData;
import net.md_5.bungee.api.ChatColor;

public class Bet {
	private int left;
	public TaskCreator taskBet;
	private double total;
	private Casino c;
	public HashMap<Player,Double> bet;
	public HashMap<Player,Boolean> isBet;
	
	public Bet(Casino c, int timeLeft) {
		this.c =c;
		bet = new HashMap<Player,Double>();
		isBet = new HashMap<Player,Boolean>();
		taskBet = null;
		left = timeLeft;
	}
	
	public void TimeLeft() {
		c.NotificatePlayers("Vous avez "+left+" secondes pour miser. Au bout de ces "+left+" secondes, une somme al�atoire vous sera"
				+ " retir�e.");
	}
	
	public boolean Ready() {
		for(Player p : c.listPlayers) {
			if(isBet.get(p) == null) {
				return false;
			}
		}
		return true;
	}
	
	public void startMise() {
		
		TimeLeft();
		taskBet = new TaskCreator(new BukkitRunnable() {
			public void run() {
				// Si le joueurs n'a pas du tout mis� ou qu'il a mis� une somme �gale � 0$
				if(--left > 0) {
					c.NotificatePlayers("Temps restant: "+left+"s");
				}else {
					if(Ready()) {
						taskBet.cancelTask();
					}else {
						for(Player p : c.listPlayers) {
							if(isBet.get(p) == null) {
								bet.put(p, 100d);
								// Retirer les sous
								isBet.put(p, true);
								p.sendMessage("Nous venons de vous retirer la somme de 100$");
							}
						}
					}
				}
				
			}
		}, 0L, 20L*3);
		
	}
	
	public double totalBet() {
		for(double b : bet.values()) {
			total += b;
		}
		return total;
	}
	
	public static void addBet(Player p, double Mise) {
		Casino c = PlayerData.getPlayerData(p.getName()).c;
		if(c != null) {
			if(c.isBetActivated) {
				if(c.s == State.STARTED) {
					if(c.b.isBet.get(p) == null) {
						// mise limit�e � 5000$
						if(Mise >= 100 && Mise <= 5000) {
							// Retirer les sous
							c.b.bet.put(p, Mise);
							c.b.isBet.put(p, true);
							p.sendMessage("Vous avez mis� "+Mise+"$. Attendez la fin du d�compte...");
							
						}else {
							p.sendMessage("Le montant � miser doit �tre compris entre 100$ et 5000$");
						}
					}else {
						p.sendMessage(ChatColor.RED+"Vous avez d�j� mis� !");
					}
				}else {
					p.sendMessage(ChatColor.DARK_RED+"Le jeu n'a pas encore d�but�.");
				}
			}else {
				p.sendMessage(ChatColor.DARK_RED+"Ce jeu ne dispose pas de syst�me de mise.");
			}
		}else {
			p.sendMessage(ChatColor.DARK_RED+"Aucun jeu en cours...");
		}
	}

}
