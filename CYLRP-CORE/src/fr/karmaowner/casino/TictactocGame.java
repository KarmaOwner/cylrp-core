package fr.karmaowner.casino;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.HashMap;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.Material;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.inventory.meta.ItemMeta;

import fr.karmaowner.common.Main;
import fr.karmaowner.common.TaskCreator;
import fr.karmaowner.data.PlayerData;

public class TictactocGame extends Casino{
	public BitSet casesT1; // V�rifier les cases remplies par le joueur j1
	public BitSet casesT2; // V�rifier les cases remplies par le joueur j2
	public TaskCreator Game;
	public Player p1, p2;
	public int tempsRestant;
	public int temps;
	public TEAM t1 = TEAM.ROUND, t2 = TEAM.CROSS; // croix ou rond
	public Player tour;
	public int[] physicCase = {
			0, 0, 0, 0, 1, 2, 0, 0, 0,
			0, 0, 0, 3, 4, 5, 0, 0, 0,
			0, 0, 0, 6, 7, 8, 0, 0, 0
	};
	
	public enum TEAM{
		CROSS(Material.BLAZE_ROD,"Croix"),ROUND(Material.SLIME_BALL,"Rond");
		private Material id;
		private String string;
		TEAM(Material id, String string) {
			this.id = id;
			this.string = string;
		}
		public Material getId() {
			return id;
		}
		public String getString() {
			return string;
		}
	}
	
	public TictactocGame(Player p) {
		super(2,10,true,true);
		tour = p;
		casesT1 = new BitSet(9);
		casesT2 = new BitSet(9);
		b = new Bet(this,0);
		p1 = p;
		tempsRestant = 10;
		temps = 10;
		inv = Bukkit.createInventory(null, 27, ChatColor.RED+"Tictactoc");
		fillInventory(0,"");
		Game = null;
	}
	
	public void startGame() {
		if(s.equals(State.WAITING)) {
			p2 = listPlayers.get(1);
			// On v�rifie que notre partenaire est pr�t
			s = State.STARTED;
			// la mise
			b.startMise();
			b.taskBet.WhenTaskFinished(new BukkitRunnable() {
				public void run() {
					fillInventory(11,ChatColor.DARK_BLUE+p1.getName());
					GameFlow();
				}
			});
		}
	}
	
	public void fillInventory(int id, String name) {
		// Jeu en cours...
		for(int i = 0; i < 3; i++) {
			for(int j = 0; j <= 8; j++) {
				int index = (i*9)+j;
					if( ( index < 3 || index > 5 ) && 
						( index < 12 || index > 14 ) &&
						( index < 21 || index > 23 )
					) {
					addItemInventory(Material.STAINED_GLASS_PANE,id,index,name);
					}
			}
		}
	}
	
	public boolean checkIfEndGame() {
		for(int i = 0; i < inv.getSize(); i++) {
			if(inv.getItem(i) == null) {
				return false;
			}
		}
		return true;
	}
	
	public void GameFlow() {
		Game = new TaskCreator(new BukkitRunnable() {
			@Override
			public void run() {
					openInventory();
					if(checkIfEndGame()) {
						endGame(null);
					}
					if(--tempsRestant <= 0) {
						for(int i = 0; i < inv.getContents().length; i++) {
							if(inv.getContents()[i] == null) {
								addItemInventory(tour.equals(p1) ? t1.getId() : t2.getId(),1,i,
										tour.equals(p1) ? t1.getString() : t2.getString());
								if(tour.equals(p1)) {
									casesT1.set(physicCase[i]);
									tour = p2;
								}else {
									casesT2.set(physicCase[i]);
									tour = p1;
								}
								if(checkIfWinner() != null) {
									endGame(checkIfWinner());
								}
								break;
							}
						}
						tempsRestant = temps;
						tour.sendMessage("C'est � votre tour !");
					}
					if(tour.equals(p1)) {
						fillInventory(11,ChatColor.DARK_BLUE+p1.getName());
					}else {
						fillInventory(14,ChatColor.DARK_RED+p2.getName());
					}
			}
		},0L,20L);
	}
	
	public void endGame(Player player) {
		if(player != null) {
			Game.cancelTask();
			Firework f = (Firework)player.getWorld().spawn(player.getLocation(), Firework.class);
			FireworkMeta effect = f.getFireworkMeta();
			effect.setPower(1);
			effect.addEffect(FireworkEffect.builder()
					.withColor(p1.equals(player) ? Color.BLUE : Color.RED)
					.flicker(true)
					.build());
			f.setFireworkMeta(effect);
			new TaskCreator(new BukkitRunnable() {
				public void run() {
					f.detonate();
					cancel();
				}
			}, 10L, 0L);
			// On r�cup�re seulement sa mise afin d'�viter les in�galit�s en terme de gain
			double miseTotale = b.bet.get(player)*2;
			NotificatePlayers("Partie termin�e: Le gagnant est "+player.getName());
			player.sendMessage("Vous venez de remporter "+miseTotale+"$");
			// envoyer au gagnant la mise totale.
		}else {
			NotificatePlayers("Partie termin�e: aucun gagnant");
			// rendre la mise.
		}
		super.endGame();
	}
	
	public Player checkIfWinner() {
		boolean pos1T1 = casesT1.get(0) && casesT1.get(1) && casesT1.get(2);
		boolean pos2T1 = casesT1.get(3) && casesT1.get(4) && casesT1.get(5);
		boolean pos3T1 = casesT1.get(6) && casesT1.get(7) && casesT1.get(8);
		boolean pos4T1 = casesT1.get(0) && casesT1.get(4) && casesT1.get(8);
		boolean pos5T1 = casesT1.get(6) && casesT1.get(4) && casesT1.get(2);
		boolean pos6T1 = casesT1.get(0) && casesT1.get(3) && casesT1.get(6);
		boolean pos7T1 = casesT1.get(1) && casesT1.get(4) && casesT1.get(7);
		boolean pos8T1 = casesT1.get(2) && casesT1.get(5) && casesT1.get(8);
		
		boolean pos1T2 = casesT2.get(0) && casesT2.get(1) && casesT2.get(2);
		boolean pos2T2 = casesT2.get(3) && casesT2.get(4) && casesT2.get(5);
		boolean pos3T2 = casesT2.get(6) && casesT2.get(7) && casesT2.get(8);
		boolean pos4T2 = casesT2.get(0) && casesT2.get(4) && casesT2.get(8);
		boolean pos5T2 = casesT2.get(6) && casesT2.get(4) && casesT2.get(2);
		boolean pos6T2 = casesT2.get(0) && casesT2.get(3) && casesT2.get(6);
		boolean pos7T2 = casesT2.get(1) && casesT2.get(4) && casesT2.get(7);
		boolean pos8T2 = casesT2.get(2) && casesT2.get(5) && casesT2.get(8);
		
		if(pos1T1 || pos2T1|| pos3T1 || pos4T1 || pos5T1 || pos6T1 || pos7T1 || pos8T1) {
			return p1;
		}else if(pos1T2 || pos2T2 || pos3T2 || pos4T2 || pos5T2 || pos6T2 || pos7T2 || pos8T2) {
			return p2;
		}
		return null;
	}
	
	public String toString() {
		return ChatColor.DARK_AQUA+"Tictactoc"+super.toString();
	}

}
