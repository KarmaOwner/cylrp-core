package fr.karmaowner.casino;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.MaterialData;
import org.bukkit.scheduler.BukkitRunnable;

import fr.karmaowner.casino.Casino.State;
import fr.karmaowner.common.Main;
import fr.karmaowner.common.TaskCreator;
import fr.karmaowner.data.PlayerData;
import net.md_5.bungee.api.ChatColor;

public abstract class Casino implements Listener{
	public State s = State.WAITING;// Etat du jeu � nul
	public Inventory inv; // Inventaire du jeu
	public int nbPlayers; // nombre de joueurs requis pour d�buter la partie
	private int WaitingTime; // temps d'attente avant le d�but de la partie
	private boolean waitEverybody;
	public Bet b; 
	public boolean isBetActivated;
	public ArrayList<Player> listPlayers; // liste des joueurs dans la partie
	public static ArrayList<Casino> waitingGames = new ArrayList<Casino>();
	private TaskCreator waiting;
	
	public enum State{
		STARTED,
		WAITING,
		END
	}
	abstract void startGame();
	public void desktroyGame() {
		waitingGames.remove(this);
		
		if(waiting != null)
		waiting.cancelTask();
	}
	
	public String toString() {
		return "-"+s+" : "+listPlayers.size()+"/"+nbPlayers;
	}
	
	public Casino(int nbPlayers, int WaitingTime, boolean waitEverybody, boolean isBetActivated) {
		// On enregistre l'�v�nement
		this.nbPlayers = nbPlayers;
		this.WaitingTime = WaitingTime;
		this.waitEverybody = waitEverybody;
		b = null;
		this.isBetActivated = isBetActivated;
	}
	
	public void endGame() {
		s = State.END;
		for(Player p : listPlayers) {
			PlayerData.getPlayerData(p.getName()).c = null;
			p.closeInventory();
		}
		waitingGames.remove(this);
	}
	
	public void TaskWaiting() {
		long Delay = 0L;
		long RepeatingTime = 20L*5; // 5 secondes
		 waiting = new TaskCreator(new BukkitRunnable() {
			public void run() {
				// Le joueur 0 de la liste est l'h�te
				if(nbPlayers == listPlayers.size() || !waitEverybody) {
					if(--WaitingTime <= 0 || waitEverybody) {
						// Lancer le jeu
						startGame();
						waiting.cancelTask();
					}else {
						for(Player p : listPlayers) {
							p.sendMessage("Temps restant avant le d�but de la partie: "+WaitingTime+"s");
						}
					}
				}else {
					for(Player p : listPlayers) {
						p.sendMessage(ChatColor.DARK_AQUA+"Recherche de partenaires en cours...");
					}
				}
			}
			
		}, Delay, RepeatingTime);
	}
	
	public void NotificatePlayers() {
		for(Player p : listPlayers) {
			p.sendMessage("nombre de joueurs dans la partie : "+listPlayers.size()+"/"+nbPlayers);
		}
	}
	
	public void NotificatePlayers(String msg) {
		for(Player p : listPlayers) {
			p.sendMessage(msg);
		}
	}
	
	
	public static void joinParty(String GameType, Player p) {
		if(PlayerData.getPlayerData(p.getName()).c == null) {
			// On cherche une partie en attente
			for(Casino c : waitingGames) {
				if(c.s == State.WAITING) {
					if(GameType.equals("Tictactoc") && c instanceof TictactocGame) {
						c.listPlayers.add(p);
						c.NotificatePlayers();
						PlayerData.getPlayerData(p.getName()).c = c;
						return;
					}else if(GameType.equals("Jackpot") && c instanceof Jackpot) {
						c.listPlayers.add(p);
						c.NotificatePlayers();
						PlayerData.getPlayerData(p.getName()).c = c;
						return;
					}else if(GameType.equals("Roulette") && c instanceof RouletteGame) {
						c.listPlayers.add(p);
						c.NotificatePlayers();
						PlayerData.getPlayerData(p.getName()).c = c;
						return;
					}
				}
			}
			// Si pas de partie trouv�e
			// On cr�e une nouvelle partie
			ArrayList<Player> listPlayers = new ArrayList<Player>();
			listPlayers.add(p);
			if(GameType.equals("Tictactoc")) {
				TictactocGame t = new TictactocGame(p);
				t.listPlayers = listPlayers;
				waitingGames.add(t);
				t.NotificatePlayers();
				t.TaskWaiting();
				PlayerData.getPlayerData(p.getName()).c = t;
			}else if(GameType.equals("Jackpot")) {
				Jackpot j = new Jackpot(p);
				j.listPlayers = listPlayers;
				waitingGames.add(j);
				j.NotificatePlayers();
				j.TaskWaiting();
				PlayerData.getPlayerData(p.getName()).c = j;
			}else if(GameType.equals("Roulette")) {
				RouletteGame r = new RouletteGame(p);
				r.listPlayers = listPlayers;
				waitingGames.add(r);
				r.NotificatePlayers();
				r.TaskWaiting();
				PlayerData.getPlayerData(p.getName()).c = r;
			}
		}else {
			p.sendMessage(ChatColor.DARK_RED+"Vous �tes d�j� dans une partie.");
		}
	}
	
	public static void StopWaiting(Player p) {
		Casino c = PlayerData.getPlayerData(p.getName()).c;
		if(c != null) {
			if(c.s == State.WAITING) {
				PlayerData.getPlayerData(p.getName()).c = null;
				if(c.listPlayers.size() == 1) {
					c.desktroyGame();
					c = null;
				}else {
					c.listPlayers.remove(p);
					c.NotificatePlayers();
				}
				p.sendMessage("Vous venez de quitter la partie.");
			}else {
				p.sendMessage("Le jeu a d�j� d�but�, vous ne pouvez pas quitter la partie.");
			}
		}else {
			p.sendMessage("Aucune partie en cours.");
		}
	}
	
	public static Casino getGame(Player p) {
		for(Casino c : waitingGames) {
			for(Player p1 : c.listPlayers) {
				if(p1.equals(p)) {
					return c;
				}
			}
		}
		return null;
	}
	
	public void addItemInventory(Material material, int id, int Slot, String name) {
		ItemStack item = new ItemStack(material,1,(byte)id);
		ItemMeta i = item.getItemMeta();
		i.setDisplayName(name);
		item.setItemMeta(i);
		inv.setItem(Slot, item);
	}
	
	public void closeInventory() {
		for(Player p : listPlayers) {
			p.closeInventory();
		}
	}
	
	// Ouvrir l'inventaire si l'�tat du jeu est � pr�t
	public void openInventory() {
		if(s == State.STARTED) {
			for(Player p : listPlayers) {
				if(p.getOpenInventory() != null 
				&& !p.getOpenInventory().getTopInventory().getName().equals(inv.getName())) 
					p.openInventory(inv);
			}
		}
	}

	public static void listParty(Player p) {
		p.sendMessage(ChatColor.BOLD+""+ChatColor.GOLD+"--------------------------------");
		p.sendMessage(ChatColor.DARK_RED+"| Nombre de partie : "+waitingGames.size());
		for(Casino c : waitingGames) {
			p.sendMessage(c.toString());
		}
		p.sendMessage(ChatColor.BOLD+""+ChatColor.GOLD+"--------------------------------");
	}
}
