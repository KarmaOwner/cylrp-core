package fr.karmaowner.casino;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import fr.karmaowner.common.Main;
import fr.karmaowner.common.TaskCreator;
import fr.karmaowner.data.PlayerData;

public class Jackpot extends Casino{

	private Player p;
	private int time = 25;
	private int timeLeft = 25;
	private int waitTime = 60;
	int next = -1;
	private TaskCreator game;
	private Material[] randomItems = {
		Material.APPLE, 
		Material.GOLDEN_APPLE, 
		Material.DIAMOND, 
		Material.TNT, 
	};
	private int[] recompenses = {
		500,
		1000,
		2000,
		3000
	};
	
	private ArrayList<Material> listItems = new ArrayList<Material>();
	
	public Jackpot(Player p) {
		super(1,0,true,false);
		this.p = p;
		inv = Bukkit.createInventory(null, InventoryType.DISPENSER, ChatColor.BLUE+"Jackpot");
		for(int i = 0; i < 18; i++) {
				int random = (int)(Math.random()*randomItems.length);
				listItems.add(randomItems[random]);
		}
		for(int i = 0; i < 3; i++) {
			for(int j = 0; j < 3; j++) {
				int item = (i*6)+j;
				int slot = (i*3)+j;
				addItemInventory(listItems.get(item), 0, slot, "");
			}
		}
	}
	
	@EventHandler
	public void onInventoryClick(InventoryClickEvent e) {
		if(e.getInventory().getName().equals(inv.getName())) {
			e.setCancelled(true);
		}
	}
	
	public void startGame() {
		//prelever une certaine somme avec essentials
		if(s == State.WAITING) {
			s = State.STARTED;
			addRandomItemInv();
		}
	}
	
	public int CheckWinner() {
		ItemStack slot1 = inv.getItem(3);
		ItemStack slot2 = inv.getItem(4);
		ItemStack slot3 = inv.getItem(5);

		for(int i = 0; i < randomItems.length; i++) {
			if(slot1.getType() == randomItems[i] &&
			   slot2.getType() == randomItems[i] &&
			   slot3.getType() == randomItems[i]) {
				return i;
			}
		}
		return -1;
	}
	
	public void endGame() {
		if(s == State.END) {
			int m = CheckWinner();
			if(m != -1) {
				int recompense = recompenses[m];
				p.sendMessage("Félicitation: Vous avez remporté "+recompense+"$");
			}else {
				p.sendMessage("Vous n'avez rien gagné !");
			}
			p.sendMessage("Fin de la partie.");
			super.endGame();
		}
	}
	
	public void addRandomItemInv() {
		 game = new TaskCreator(new BukkitRunnable() {
			public void run() {
					openInventory();
					if(--timeLeft <= 0) {
						next++;
						timeLeft = time;
					}
						
					if(next < 0) {
						// column one
						for(int j = 0; j < 3; j++) {
							int item = ((0*6)+j+(time-timeLeft))%18;
							int slot = 0+(j*3);
							addItemInventory(listItems.get(item), 0, slot, "");
						}
					}
						
						if(next < 1) {
							// column two
							for(int j = 0; j < 3; j++) {
								int item = ((1*6)+j+(time-timeLeft))%18;
								int slot = 1+(j*3);
								addItemInventory(listItems.get(item), 0, slot, "");
							}
						}
						
						if(next < 2) {
							// column three
							for(int j = 0; j < 3; j++) {
								int item = ((2*6)+j+(time-timeLeft))%18;
								int slot = 2+(j*3);
								addItemInventory(listItems.get(item), 0, slot, "");
							}
						}else {
							if(--waitTime <= 0) {
								game.cancelTask();
								s = State.END;
								endGame();
							}
						}
			}
		},0L,3L);
	}
	
	public String toString() {
		return ChatColor.RED+"Jackpot"+super.toString();
	}
	
}
