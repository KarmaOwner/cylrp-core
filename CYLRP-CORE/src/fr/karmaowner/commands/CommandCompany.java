package fr.karmaowner.commands;

import java.io.File;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.karmaowner.common.Main;
import fr.karmaowner.common.Request;
import fr.karmaowner.common.Request.RequestType;
import fr.karmaowner.data.CompanyData;
import fr.karmaowner.data.PlayerData;
import fr.karmaowner.utils.MessageUtils;

public class CommandCompany implements CommandExecutor {
	
	PlayerData pData;
	Player pSender;

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(sender instanceof Player)
			pSender = (Player) sender;
		
		pData = PlayerData.getPlayerData(pSender.getName());
		
		if(cmd.getName().equalsIgnoreCase("entreprise")) {
			if(args.length < 1) {
				displayHelp(pSender, 1);
				return true;
			}
			else {
				if(args.length == 1) 
				{			
					if(args[0].equalsIgnoreCase("help")) {
						displayHelp(pSender, 1);
					}
					else if(args[0].equalsIgnoreCase("classement")) {
						
					}
					else if(args[0].equalsIgnoreCase("leave")) {
						if(CompanyData.Companies.containsKey(pData.companyName)) {
							CompanyData cData = CompanyData.Companies.get(pData.companyName);
							if(cData.getGerant().equalsIgnoreCase(pSender.getName()))
							{
								MessageUtils.sendMessageFromConfig(pSender, "company-owner");
								return true;
							}
							cData.onLeft(pSender);
						}
						else
							MessageUtils.sendMessageFromConfig(pSender, "company-havent");
					}
				}
				else if(args.length == 2) 
				{
					if(args[0].equalsIgnoreCase("help")) 
					{
						int arg1 = 0;
						try {
							arg1 = Integer.parseInt(args[1]);
						}
						catch(NumberFormatException e) {
							MessageUtils.getMessageFromConfig("not-integer");
						}
						finally {
							displayHelp(pSender, arg1);
						}
					}
					else if(args[0].equalsIgnoreCase("rank")) 
					{
						if(args[1].equalsIgnoreCase("list"))
						{
							displayRankList(pSender);
						}
					}
					else if(args[0].equalsIgnoreCase("salaire"))
					{
						if(args[1].equalsIgnoreCase("list"))
						{
							if(!CompanyData.Companies.containsKey(pData.companyName)) {
								MessageUtils.sendMessageFromConfig(pSender, "company-havent");
								return false;
							}
							
							CompanyData.Companies.get(pData.companyName).displayRepartition(pSender);
						}
					}
					else if(args[0].equalsIgnoreCase("invite")) 
					{
						
						
						if(args[1].equalsIgnoreCase(pSender.getName())) return false;
						
						if(!CompanyData.Companies.containsKey(pData.companyName)) {
							MessageUtils.sendMessageFromConfig(pSender, "company-havent");
							return false;
						}
						
						CompanyData cData = CompanyData.Companies.get(pData.companyName);
								
						if(args[1].isEmpty() || Bukkit.getPlayer(args[1]) == null) 
						{
							MessageUtils.sendMessageFromConfig(pSender, "company-user-invalid");
							return false;
						}
						
						if(cData.getGerant().equalsIgnoreCase(pSender.getName()) || cData.getCoGerant().contains(pSender.getName()))
						{	
							if(cData.getUsersList().contains(args[1]))
							{
								MessageUtils.sendMessageFromConfig(pSender, "company-user-exist");
								return false;
							}
							new Request(RequestType.COMPANY, pSender.getName(), args[1], cData.getCompanyName());
						}
						else
							MessageUtils.sendMessageFromConfig(pSender, "company-less-permission");
					}
					else if(args[0].equalsIgnoreCase("join")) 
					{
						if(args[1].equalsIgnoreCase(pSender.getName())) return false;
						
						if(args[1].isEmpty()) 
						{
							MessageUtils.sendMessageFromConfig(pSender, "company-user-invalid");
							return false;
						}
						Request request = Request.findRequest(args[1]);
						if(request == null) {
							MessageUtils.sendMessageFromConfig(pSender, "company-request-invalid");
							return false;
						}
						
						request.getCompanyData().addUser(pSender);
					}
					else if(args[0].equalsIgnoreCase("kick")) 
					{
						if(!CompanyData.Companies.containsKey(pData.companyName))
						{
							MessageUtils.sendMessageFromConfig(pSender, "company-havent");
							return false;
						}
						
						CompanyData cData = CompanyData.Companies.get(pData.companyName);
						if(args[1].isEmpty()) {
							MessageUtils.sendMessage(pSender, "company-enter-username");
							return false;
						}
						if(cData.getGerant().equalsIgnoreCase(pSender.getName()) || cData.getCoGerant().contains(pSender.getName()))
						{
							if(!cData.getUsersList().contains(args[1]))
							{
								MessageUtils.sendMessage(pSender, "company-not-contain-username");
								return false;
							}
							cData.onKick(pSender.getName(), args[1]);
							return true;
						}
						else
							MessageUtils.sendMessageFromConfig(pSender, "company-less-permission");
					}
					else if(args[0].equalsIgnoreCase("publicite")) 
					{
						if(!CompanyData.Companies.containsKey(pData.companyName)) {
							MessageUtils.sendMessageFromConfig(pSender, "havent-company");
							return false;
						}
						
						if(args[1].isEmpty()) {
							MessageUtils.sendMessageFromConfig(pSender, "message-enter");
							return false;
						}
						
						CompanyData cData = CompanyData.Companies.get(pData.companyName);
						
						if(!(cData.getGerant().equalsIgnoreCase(pSender.getName()) || cData.getCoGerant().contains(pSender.getName()) || cData.getCommunityManagers().contains(pSender.getName())))
						{
							MessageUtils.sendMessageFromConfig(pSender, "company-less-permission");
							return false;
						}
							
						if(!pData.commandConfirmation) 
						{
							MessageUtils.sendMessage(pSender, "§4Attention ! §cLa publicité coûtera §610$ §cpar joueurs connectés");
							MessageUtils.sendMessage(pSender, "§cRéutiliser la commande pour confirmer");
							pData.commandConfirmation = true;
							return false;
						}
						pData.commandConfirmation = false;
						MessageUtils.sendMessage(pSender, "§aTransaction comfirmé");
						int price = Bukkit.getServer().getOnlinePlayers().length * 10;
						//utiliser essentials pour retirer de l'argent au joueur
					}
					else if(args[0].equalsIgnoreCase("stockage")) 
					{
						//Bukkit.getServer().
					}
					else if(args[0].equalsIgnoreCase("announce"))
					{
						if(!CompanyData.Companies.containsKey(pData.companyName)) {
							MessageUtils.sendMessageFromConfig(pSender, "company-havent");
							return false;
						}
						
						if(args[1].isEmpty())
						{
							MessageUtils.sendMessageFromConfig(pSender, "message-enter");
							return false;
						}
						
						CompanyData cData = CompanyData.Companies.get(pData.companyName);
						
						if(!(cData.getGerant().equalsIgnoreCase(pSender.getName()) || cData.getCoGerant().contains(pSender.getName()) || cData.getCommunityManagers().contains(pSender.getName()) || cData.getSecretaires().contains(pSender.getName())))
						{
							cData.broadcastCompany(args[1]);
						}
						
					}
				}
				else if(args.length == 3) {
					if(args[0].equalsIgnoreCase("setrank")) {
						if(args[1].isEmpty() || args[2].isEmpty()) {
							MessageUtils.sendMessageFromConfig(pSender, "command-invalid");
							displayHelp(pSender, 1);
							return false;
						}
						
						CompanyData cData = CompanyData.Companies.get(pData.companyName);
						
						if(args[2].equalsIgnoreCase("CoGerant"))
						{
							if(!(cData.getGerant().equalsIgnoreCase(pSender.getName()) || cData.getCoGerant().contains(pSender.getName()))) 
							{
								MessageUtils.sendMessageFromConfig(pSender, "company-less-permission");
								return false;
							}
							
								
							if(cData.getCoGerant().contains(args[1])) 
							{
								MessageUtils.sendMessageFromConfig(pSender, "company-already-rank");
								return false;
							}
							
							cData.setRank(args[1], args[2]);
							
							
						}
						else if((args[2].equalsIgnoreCase("Secretaire") || args[2].equalsIgnoreCase("CommunityManager") || args[2].equalsIgnoreCase("Stagiaire") || args[2].equalsIgnoreCase("Salarie"))) 
						{							
							if(!(cData.getGerant().equalsIgnoreCase(pSender.getName()) || cData.getCoGerant().contains(pSender.getName()))) 
							{
								MessageUtils.sendMessageFromConfig(pSender, "company-less-permission");
								return false;
							}
							
							if(cData.getCommunityManagers().contains(args[1]) || cData.getSecretaires().contains(args[1]) || cData.getStagiaires().contains(args[1])) 
							{
								MessageUtils.sendMessageFromConfig(pSender, "company-already-rank");
								return false;
							}
							
							cData.setRank(args[1], args[2]);
							
						}
						else 
						{
							displayRankList(pSender);
						}
				 }
				 else if(args[0].equalsIgnoreCase("salaire"))
				 {
					if(args[1].isEmpty() || args[2].isEmpty())
					{
						MessageUtils.sendMessageFromConfig(pSender, "command-invalid");	
						return false;
					}
					 
					if(!CompanyData.Companies.containsKey(pData.companyName))
					{
						MessageUtils.sendMessageFromConfig(pSender, "company-havent");
						return false;
					}
					
					CompanyData cData = CompanyData.Companies.get(pData.companyName);
					
					if(CompanyData.rankExist(args[1]))
					{
						int arg1 = 0;
						try {
							arg1 = Integer.parseInt(args[1]);
						}
						catch(NumberFormatException e) {
							MessageUtils.sendMessageFromConfig(pSender, "not-integer");
							return false;
						}
						
						if(cData.setRepartition(args[1], arg1))
							return true;	
						
						MessageUtils.sendMessageFromConfig(pSender, "company-repartition-max");
					}
				 }		
			}
			else if(args.length == 4)
			{
				if(args[0].equalsIgnoreCase("create")) 
				{
					Player p = Bukkit.getPlayer(args[1]);
					if(pSender == null) {
						if(!(new File(Main.INSTANCE.getDataFolder() + File.separator + "Entreprises" + File.separator + args[1] + ".yml").exists()))
						{
							new CompanyData(args[2], p, args[3]);
							Bukkit.broadcastMessage("Entreprise crée");
						}
						else
							MessageUtils.sendMessage(pSender, MessageUtils.getMessageFromConfig("company-exist"));
						return false;
					}
					
					if(!pSender.hasPermission("cylrp.company.create"))
					{
						MessageUtils.sendMessageFromConfig(pSender, "cylrp-not-permission");
						return false;
					}
					
					if(!args[1].isEmpty() && !args[2].isEmpty() && !args[3].isEmpty())
						if(PlayerData.getPlayerData(p.getName()).companyName == null)
							if(!(new File(Main.INSTANCE.getDataFolder() + File.separator + "Entreprises" + File.separator + args[2] + ".yml").exists()))
							{
								new CompanyData(args[2], p, args[3]);
								Bukkit.broadcastMessage("Entreprise crée");
								Bukkit.broadcastMessage(PlayerData.getPlayerData(p.getName()).companyName);
							}
							else
								MessageUtils.sendMessage(pSender, MessageUtils.getMessageFromConfig("company-exist"));
						else
							MessageUtils.sendMessage(pSender, MessageUtils.getMessageFromConfig("company-owned"));
					else 
					{
						MessageUtils.sendMessage(pSender, MessageUtils.getMessageFromConfig("command-invalid"));
						displayHelp(pSender, 1);
					}
				}
		   }
		}
	}
	return false;
	
}
	
	
	public void displayHelp(CommandSender sender , int page) {
		if(page == 1) {
			sender.sendMessage("§b【Entreprise commands 1/4 】");
			sender.sendMessage("");
			sender.sendMessage("● §c/entreprise help <Page> §7- §aAffiche la liste des commandes");
			sender.sendMessage("● §c/entreprise classement §7- §aAffiche le classement des entreprises");
			sender.sendMessage("● §c/entreprise leave §7- §aPermet de quitter son entreprise");
			sender.sendMessage("● §c/entreprise invite <Pseudo> §7- §aInviter un joueur dans son entreprise");
			sender.sendMessage("● §c/entreprise join <Nom Entreprise> §7- §aRejoindre un entreprise si la requête existe");
		}
		else if(page == 2) {
			sender.sendMessage("§b【Entreprise commands 2/4 】");
			sender.sendMessage("");
			sender.sendMessage("● §c/entreprise invite <Pseudo> §7- §aInviter un joueur dans son entreprise");
			sender.sendMessage("● §c/entreprise join <Nom Entreprise> §7- §aRejoindre un entreprise si la requête existe");
			sender.sendMessage("● §c/entreprise kick <Pseudo> §7- §aVirer un joueur de son entreprise");
			sender.sendMessage("● §c/entreprise publicite <Message> §7- §aFaire une publicité dans le chat");
			sender.sendMessage("● §c/entreprise setrank <Pseudo> <Rang> §7- §aOuvre un panel de gestion pour gérer son entreprise");
		}
		else if(page == 3) {
			sender.sendMessage("§b【Entreprise commands 3/4 】");
			sender.sendMessage("");
			sender.sendMessage("● §c/entreprise salaire <Rang> <Repartition en pourcentage> §7- §aPermet de répartir le pourcentage du salaire total qui sera fournie en fin de semaine");
			sender.sendMessage("● §c/entreprise stockage add §7- §aAjoute l'item en main dans le stockage de l'entreprise");
			sender.sendMessage("● §c/entreprise stockage remove <id> §7- §aRetire un item du stockage à partir de l'id");
			sender.sendMessage("● §c/entreprise announce <message> §7- §aPermet de faire une annonce");
			sender.sendMessage("●§ c/entreprise rank list §7- §aPermet d'avoir la liste des grades");
		}
		else if(page == 4) {
			sender.sendMessage("§b【Entreprise commands 4/4 】");
			sender.sendMessage("");
			sender.sendMessage("● §c/entreprise salaire list §7- §aAffiche la répartition en pourcentage pour chaque grades");
			sender.sendMessage("● §c/entreprise create <pseudo> <nom entreprise> <categorie> list §7- §aAffiche la répartition en pourcentage pour chaque grades");
		}
	}
	
	public void displayRankList(CommandSender sender) {
		sender.sendMessage("§7► §bEntreprise Rangs §7◄");
		sender.sendMessage("§fGerant ◊ CoGerant ◊ CommunityManager ◊ Secretaire ◊ Stagiaires ◊ Salarie");
	}

}
