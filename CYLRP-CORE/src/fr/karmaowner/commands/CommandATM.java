package fr.karmaowner.commands;

import java.math.BigDecimal;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import fr.karmaowner.data.PlayerData;
import fr.karmaowner.utils.MessageUtils;
import fr.karmaowner.utils.MoneyUtils;

public class CommandATM implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(cmd.getName().equalsIgnoreCase("atm"))
		{
			
			if(args.length == 2) {
				if(sender instanceof Player) {
					Player p = (Player) sender;
					PlayerData pData = PlayerData.getPlayerData(p.getName());
					
					
					if(args[0].equalsIgnoreCase("put")) 
					{
						int value;
						try {
							value = Integer.parseInt(args[1]);
						}
						catch(NumberFormatException e)
						{
							MessageUtils.sendMessageFromConfig(p, "not-integer");
							return false;
						}
						//pData.setMoney(pData.getMoney().add(BigDecimal.valueOf(value)));
						if(MoneyUtils.addMoneyToBank(value, p))
							MessageUtils.sendMessageFromConfig(p, "atm-put-successfuly");
						else
							MessageUtils.sendMessageFromConfig(p, "atm-enough-tickets");
					}
					else if(args[0].equalsIgnoreCase("take"))
					{
						int value;
						try {
							value = Integer.parseInt(args[1]);
						}
						catch(NumberFormatException e)
						{
							MessageUtils.sendMessageFromConfig(p, "not-integer");
							return false;
						}
						/*if(pData.getMoney().intValue() + 1 >= value)
						{
							pData.setMoney(pData.getMoney().subtract(BigDecimal.valueOf(value)));						
							MoneyUtils.convertValueToTickets(value, p);
							MessageUtils.sendMessageFromConfig(p, "atm-take-successfuly");
						}
						else
							MessageUtils.sendMessageFromConfig(p, "atm-enough-money");*/
					}

					
				}
			}
			else if(args.length == 4) {
				if(sender instanceof Player) {
					Player p = (Player) sender;
					
					if(args[0].isEmpty() || args[1].isEmpty() || args[2].isEmpty() || args[3].isEmpty())
					{
					
						MessageUtils.sendMessageFromConfig(p, "command-invalid");
						return false;
					}
					
					if(args[0].equalsIgnoreCase("admin")) 
					{
						p = Bukkit.getPlayer(args[2]);
						if(p == null)
						{
							MessageUtils.sendMessageFromConfig(p, "user-invalid");
							return false;
						}
						
						PlayerData pData = PlayerData.getPlayerData(p.getName());
						
						if(args[1].equalsIgnoreCase("put") && sender.hasPermission("cylrp.atm.put"))
						{	
							BigDecimal bd;
							try {
								args[1].replaceAll(",","");
								bd = new BigDecimal(args[1]);
							}
							catch(NumberFormatException e)
							{
								MessageUtils.sendMessageFromConfig(p, "bad-number");
								return false;
							}
							//pData.setMoney(pData.getMoney().add(bd));
						}
						else if(args[1].equalsIgnoreCase("take") && sender.hasPermission("cylrp.atm.take"))
						{
							BigDecimal bd;
							try {
								args[1].replaceAll(",","");
								bd = new BigDecimal(args[1]);
							}
							catch(NumberFormatException e)
							{
								MessageUtils.sendMessageFromConfig(p, "bad-number");
								return false;
							}
							//pData.setMoney(pData.getMoney().subtract(bd));
						}
						else if(!(sender.hasPermission("cylrp.atm.take") || sender.hasPermission("cylrp.atm.put"))) {
							MessageUtils.sendMessageFromConfig(sender, "cylrp-not-permission");
						}
					}
						
				}
				else 
				{
					Player p = Bukkit.getPlayer(args[2]);
					if(p == null)
					{
						MessageUtils.sendLogFromConfig("user-invalid");
						return false;
					}
					
					PlayerData pData = PlayerData.getPlayerData(p.getName());
					
					if(args[1].equalsIgnoreCase("put") && sender.hasPermission("cylrp.atm.put"))
					{	
						BigDecimal bd;
						try {
							args[1].replaceAll(",","");
							bd = new BigDecimal(args[1]);
						}
						catch(NumberFormatException e)
						{
							MessageUtils.sendLogFromConfig("bad-number");
							return false;
						}
						//pData.setMoney(pData.getMoney().add(bd));
					}
					else if(args[1].equalsIgnoreCase("take") && sender.hasPermission("cylrp.atm.take"))
					{
						BigDecimal bd;
						try {
							args[1].replaceAll(",","");
							bd = new BigDecimal(args[1]);
						}
						catch(NumberFormatException e)
						{
							MessageUtils.sendLogFromConfig("bad-number");
							return false;
						}
						//pData.setMoney(pData.getMoney().subtract(bd));
					}
					else if(!(sender.hasPermission("cylrp.atm.take") || sender.hasPermission("cylrp.atm.put"))) {
						MessageUtils.sendLogFromConfig("cylrp-not-permission");
					}
				}
			}
		
		}
		return false;
	}

}
