package fr.karmaowner.commands;

import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import fr.karmaowner.casino.Bet;
import fr.karmaowner.casino.Casino;
import fr.karmaowner.casino.Casino.State;
import fr.karmaowner.casino.Jackpot;
import net.md_5.bungee.api.ChatColor;
import fr.karmaowner.casino.TictactocGame;
import fr.karmaowner.data.PlayerData;

public class CommandCasino implements CommandExecutor{
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(sender instanceof Player) {
			Player p = (Player)sender;
			
			if(cmd.getName().equalsIgnoreCase("casino") &&
					args.length == 1 &&
					args[0].equalsIgnoreCase("tictactoc")) {
				Casino.joinParty("Tictactoc", p);
			}else if(cmd.getName().equalsIgnoreCase("casino") &&
					args.length == 1 &&
					args[0].equalsIgnoreCase("stopAttendre")) {
				Casino.StopWaiting(p);
			}else if(cmd.getName().equalsIgnoreCase("casino") &&
					args.length == 2 && 
					args[0].equalsIgnoreCase("miser") && !args[1].isEmpty()) {
					Bet.addBet(p,Double.parseDouble(args[1]));
			}else if(cmd.getName().equalsIgnoreCase("casino") 
					&& args.length == 1 && args[0].equals("jackpot")) {
				Casino.joinParty("Jackpot", p);
			}else if(cmd.getName().equalsIgnoreCase("casino") 
					&& args.length == 1 && args[0].equals("roulette")) {
				Casino.joinParty("Roulette", p);
			}else if(cmd.getName().equalsIgnoreCase("casino") 
					&& args.length == 1 && args[0].equals("partie")) {
				Casino.listParty(p);
			}
		}
		
		return false;
	}
}
