package fr.karmaowner.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.karmaowner.utils.FileUtils;
import fr.karmaowner.utils.MessageUtils;
import fr.karmaowner.common.Achievements;
import fr.karmaowner.companies.Company;

public class CompanyData extends FileUtils implements DataAchievements{
	
	public static HashMap<String,CompanyData> Companies = new HashMap<String,CompanyData>();
	
	private Company company;
	
	private String CompanyName = "";
	private String Categorie = "";

	private String Gerant = "";
	private List<String> Secretaires, CommunityManagers, Stagiaires, CoGerant;
	private List<String> UsersList;
	private List<Achievements> achievements = new ArrayList<Achievements>();
	
	// Le compteur est l'unit� repr�sentative d'une activit� de l'entreprise
	// Par exemple: pour le bucheron c'est le nombre de bois.
	// Il sert principalement � attribuer des r�ussites
	private final double ONELEVELXP = 500d;
	
	private int Revenues;
	private double xpToReachForLevelUp = ONELEVELXP;
	private double xpActually;
	private int Level = 1;

	
	private int UsersRepartition = 0;
	private int StagiairesRepartition = 0;
	private int SecretairesRepartition = 0;
	private int CommunityManagersRepartition = 0;
	private int CoGerantRepartition = 0;
	private int GerantRepartition = 0;
	
	public boolean isCreation = false;
	
	public CompanyData(String CompanyName, Player p, String categorie) {
		super(CompanyName,"Entreprises");
		Secretaires = new ArrayList<String>();
		CommunityManagers = new ArrayList<String>();
		Stagiaires = new ArrayList<String>();
		CoGerant = new ArrayList<String>();
		UsersList = new ArrayList<String>();
		this.CompanyName = CompanyName;

		if(!this.directoryExist()) {
			isCreation = true;
			this.createFile();
			this.setGerant(p.getName());
			UsersList.add(p.getName());
			PlayerData pData = PlayerData.getPlayerData(this.getGerant());
			this.CompanyName = CompanyName;
			this.Categorie = categorie;
			pData.companyName = CompanyName;
			pData.companyCategory = categorie;
			Companies.put(getCompanyName(), this);
			company = Company.getInstanceByCategorie(Categorie,this);
		}
		this.loadFileConfiguration();
		
		if(this.fileConfiguration != null && !isCreation) {
			loadData();
			company = Company.getInstanceByCategorie(Categorie,this);
		}
		
	}
	
	
	public Company getCompany() {
		return company;
	}
	
	
	public void loadData() {
		Gerant = this.fileConfiguration.getString("Gerant");
		CoGerant = this.fileConfiguration.getStringList("CoGerant");
		Secretaires = this.fileConfiguration.getStringList("Secretaires");
		CommunityManagers = this.fileConfiguration.getStringList("CommunityManagers");
		Stagiaires = this.fileConfiguration.getStringList("Stagiaires");
		UsersList = this.fileConfiguration.getStringList("Users");
		
		UsersRepartition = this.fileConfiguration.getInt("UsersRepartition");
		StagiairesRepartition =this.fileConfiguration.getInt("StagiairesRepartition");
		SecretairesRepartition = this.fileConfiguration.getInt("SecretairesRepartition");
		CommunityManagersRepartition = this.fileConfiguration.getInt("CommunityManagers");
		CoGerantRepartition = this.fileConfiguration.getInt("CoGerantRepartition");
		GerantRepartition = this.fileConfiguration.getInt("GerantRepartition");
		
		Revenues = this.fileConfiguration.getInt("Revenues");
		Level = this.fileConfiguration.getInt("Level");
		xpToReachForLevelUp = this.fileConfiguration.getDouble("xpToReachForLevelUp");
		xpActually = this.fileConfiguration.getDouble("xpActually");
		Categorie = this.fileConfiguration.getString("Categorie");
		CompanyName = this.fileConfiguration.getString("Name", CompanyName);
		achievements = Achievements.parseToAchievements(this.fileConfiguration.getStringList("Achievements"));
	}
	
	public void saveData() {
		if(CoGerant != null)
			this.fileConfiguration.set("CoGerant", CoGerant);
		if(Secretaires != null)
			this.fileConfiguration.set("Secretaires", Secretaires);
		if(CommunityManagers != null)
			this.fileConfiguration.set("CommunityManagers", CommunityManagers);
		if(Stagiaires != null)
			this.fileConfiguration.set("Stagiaires", Stagiaires);
		if(Gerant != null)
			this.fileConfiguration.set("Gerant", this.getGerant());
		if(UsersList != null)
			this.fileConfiguration.set("Users", UsersList);

		this.fileConfiguration.set("UsersRepartition", UsersRepartition);
		this.fileConfiguration.set("StagiairesRepartition", StagiairesRepartition);
		this.fileConfiguration.set("SecretairesRepartition", SecretairesRepartition);
		this.fileConfiguration.set("CommunityManagers", CommunityManagers);
		this.fileConfiguration.set("CoGerantRepartition", CoGerantRepartition);
		this.fileConfiguration.set("GerantRepartition", GerantRepartition);

		
		this.fileConfiguration.set("Revenues", Revenues);
		this.fileConfiguration.set("Level", Level);
		this.fileConfiguration.set("xpToReachForLevelUp", xpToReachForLevelUp);
		this.fileConfiguration.set("xpActually", xpActually);
		this.fileConfiguration.set("Categorie", Categorie);
		this.fileConfiguration.set("Name", CompanyName);
		this.fileConfiguration.set("achievements", achievements);
		CompanyData.Companies.remove(getCompanyName());
		this.saveConfig();
	}
	
	public void setXp(double xp) {
		if(xpActually+xp >= xpToReachForLevelUp) {
			Level++;
			MessageUtils.broadcast("§6Vous venez d'augmenter d'un niveau §e-> §c" + Level);
			xpToReachForLevelUp += getCoeff()*ONELEVELXP;
		}
		xpActually += xp;
	}
	
	public double getCoeff() {
		return Math.pow(Math.sqrt(Level),2);
	}
	
	public List<Achievements> getAchievements(){
		return achievements;
	}
	
	public void setAchievements(Achievements a) {
		achievements.add(a);
	}
	
	public int getLevelReached() {
		return Level;
	}
	
	public String getGerant() {
		return Gerant;
	}
	
	public void setGerant(String playerName) {
		this.Gerant = playerName;
	}
	
	public List<String> getSecretaires() {
		return Secretaires;
	}
	
	public int getRevenues() {
		return Revenues;
	}
	
	public List<String> getStagiaires() {
		return Stagiaires;
	}
	
	public List<String> getCoGerant() {
		return CoGerant;
	}
	
	public List<String> getCommunityManagers() {
		return CommunityManagers;
	}
	
	public List<String> getUsersList() {
		return UsersList;
	}
	
	public String getCompanyName() {
		return CompanyName;
	}
	
	public String getCategoryName() {
		return Categorie;
	}
	
	public void addUser(Player p) {
		this.UsersList.add(p.getName());
		broadcastCompany(MessageUtils.getMessageFromConfig("company-join").replaceAll("%player%", p.getName()));
	}
	
	public void broadcastCompany(String msg) {
		for(String user : UsersList) {
			Player pUser = Bukkit.getPlayer(user);
			if(pUser != null)
				pUser.sendMessage("§7[§c" + getCompanyName() + "§7] " + msg);
		}
	}
	
	public void setRank(String rank, String username) {
		if(rank.equalsIgnoreCase("CoGerant"))
			getCoGerant().add(username);
		else if(rank.equalsIgnoreCase("CommunityManager"))
			getCommunityManagers().add(username);
		else if(rank.equalsIgnoreCase("Stagiaire"))
			getStagiaires().add(username);
		else if(rank.equalsIgnoreCase("Secretaire"))
			getSecretaires().add(username);
		else if(rank.equalsIgnoreCase("Salarie"))
			resetRank(username);
	}
	
	public void onLeft(Player p) {
		getRank(p.getName()).remove(p.getName());
		PlayerData.getPlayerData(p.getName()).companyName = null;
		broadcastCompany(MessageUtils.getMessageFromConfig("company-left").replaceAll("%player%", p.getName()));		
	}
	
	public void onKick(String kicker, String victim) {
		getRank(victim).remove(victim);
		if(Bukkit.getPlayer(victim) != null)
		{
			String msg = MessageUtils.getMessageFromConfig("kicked-from-company");
			msg = msg.replaceAll("%kicker%", kicker);
			msg = msg.replaceAll("%company%", getCompanyName());
			MessageUtils.sendMessage(Bukkit.getPlayer(victim), msg);
		}
		String msg = MessageUtils.getMessageFromConfig("kicked-successfuly");
		msg = msg.replaceAll("%victim%", victim);
		MessageUtils.sendMessage(Bukkit.getPlayer(kicker), msg);
		
		PlayerData.getPlayerData(victim).companyName = null;
	}
	
	public void resetRank(String victim) 
	{
		getRank(victim).remove(victim);
	}
	
	public List<String> getRank(String username)
	{
		if(getGerant().equalsIgnoreCase(username))
		{
			List<String> Gerant = new ArrayList<String>();
			Gerant.add(username);
			return Gerant;
		}
		else if(getCoGerant().contains(username))
			return getCoGerant();
		else if(getCommunityManagers().contains(username))
			return getCommunityManagers();
		else if(getSecretaires().contains(username))
			return getSecretaires();
		else if(getStagiaires().contains(username))
			return getStagiaires();
		return getUsersList();
	}
	
	public int totalRepartition()
	{
		return UsersRepartition + StagiairesRepartition + SecretairesRepartition + CommunityManagersRepartition + CoGerantRepartition + GerantRepartition;
	}
	
	public boolean setRepartition(String rank, int repartition) {
		int temp;
		if(rank.equalsIgnoreCase("Gerant")) {
			temp = GerantRepartition;
			GerantRepartition = 0;
			if(totalRepartition() + repartition > 100)
			{
				GerantRepartition = temp;
				return false;
			}
			return true;
		}
		else if(rank.equalsIgnoreCase("CoGerant"))
		{
			temp = CoGerantRepartition;
			CoGerantRepartition = 0;
			if(totalRepartition() + repartition > 100)
			{
				CoGerantRepartition = temp;
				return false;
			}
			return true;
		}
		else if(rank.equalsIgnoreCase("CommunityManager"))
		{
			temp = CommunityManagersRepartition;
			CommunityManagersRepartition = 0;
			if(totalRepartition() + repartition > 100)
			{
				CommunityManagersRepartition = temp;
				return false;
			}
			return true;
		}
		else if(rank.equalsIgnoreCase("Stagiaire"))
		{
			temp = StagiairesRepartition;
			StagiairesRepartition = 0;
			if(totalRepartition() + repartition > 100)
			{
				StagiairesRepartition = temp;
				return false;
			}
			return true;
		}
		else if(rank.equalsIgnoreCase("Secretaire"))
		{
			temp = SecretairesRepartition;
			SecretairesRepartition = 0;
			if(totalRepartition() + repartition > 100)
			{
				SecretairesRepartition = temp;
				return false;
			}
			return true;
		}
		else if(rank.equalsIgnoreCase("Salarie"))
		{
			temp = UsersRepartition;
			UsersRepartition = 0;
			if(totalRepartition() + repartition > 100)
			{
				UsersRepartition = temp;
				return false;
			}
			return true;
		}
		return false;
	}
	
	public void displayRepartition(CommandSender sender) {
		sender.sendMessage("§7► §bEntreprise Repartition §7◄");
		sender.sendMessage("§4- Gerant : &c" + GerantRepartition + "%");
		sender.sendMessage("§c- CoGerants : &c" + GerantRepartition + "%");
		sender.sendMessage("§d- CommunityManagers : &c" + CommunityManagersRepartition + "%");
		sender.sendMessage("§a- Secretaires : &c" + SecretairesRepartition + "%");
		sender.sendMessage("§b- Stagiaires : &c" + StagiairesRepartition + "%");
		sender.sendMessage("§b- Salaries : &c" + UsersRepartition + "%");
	}
	
	public static boolean rankExist(String rank) {
		return rank.equalsIgnoreCase("Gerant") || rank.equalsIgnoreCase("CoGerant") || rank.equalsIgnoreCase("CommunityManager") || rank.equalsIgnoreCase("Secretaire") || rank.equalsIgnoreCase("Stagiaire") || rank.equalsIgnoreCase("Salarie");
	}
	
	public static void SaveDatas() {
		for(Entry<String, CompanyData> entry : CompanyData.Companies.entrySet())
		    entry.getValue().saveData();
	}
	
}
