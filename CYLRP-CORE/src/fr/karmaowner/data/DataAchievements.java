package fr.karmaowner.data;

import java.util.List;

import fr.karmaowner.common.Achievements;

public interface DataAchievements extends Data{
	List<Achievements> getAchievements();
	void setAchievements(Achievements a);
}
