package fr.karmaowner.data;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.earth2me.essentials.User;

import fr.karmaowner.casino.Casino;
import fr.karmaowner.common.Main;
import fr.karmaowner.jobs.Jobs;
import fr.karmaowner.utils.FileUtils;
import net.ess3.api.MaxMoneyException;

public class PlayerData extends FileUtils implements Data{
	
	private static HashMap<String, PlayerData> playersData = new HashMap<String, PlayerData>();
	
	public Timestamp lastConnectionDate;
	public String connectionIP;
	
	public String companyName = "";
	public String companyCategory = "";

	public Jobs selectedJob = Jobs.getDefaultJob();
	
	public boolean commandConfirmation = false;
	public Casino c = null;

	/**
	 * si le fichier pseudo.yml n'existe pas on cr�e un fichier yml du joueur
	 * on charge les donn�es du joueurs
	 * @param player Pseudo du joueur
	 */
	public PlayerData(Player player) {
		super(player);
		
		if(PlayerData.playersData.containsKey(player.getName())) return;
		
		if(!this.directoryExist()) this.createFile();
		
		this.loadFileConfiguration();
				
		if(this.fileConfiguration != null) {
			this.loadData();
			if(companyName != null)
				if(!CompanyData.Companies.containsKey(companyName))
					CompanyData.Companies.put(companyName, new CompanyData(companyName,player,companyCategory));
			PlayerData.playersData.put(this.player.getName(), this);
		}
	}
	

	public void loadData() {
		lastConnectionDate = new Timestamp(System.currentTimeMillis());
		connectionIP = this.player.getAddress().toString();
		companyName = this.fileConfiguration.getString("data.companyName");
		companyCategory = this.fileConfiguration.getString("data.companyCategory");
		this.player.sendMessage("�cVotre m�tier : �d" + selectedJob.getName());
	}
	
	/*** Sauvegarde les donn�es du joueur dans un fichier yml se situant dans le dossier UserData  ***/
	public void saveData() {
		this.fileConfiguration.set("data.lastconnection", lastConnectionDate.toString());
		this.fileConfiguration.set("data.selectedJob", selectedJob.getName());
		if(CompanyData.Companies.containsKey(this.companyName))
		{
			this.fileConfiguration.set("data.companyName", CompanyData.Companies.get(this.companyName).getCompanyName());
			this.fileConfiguration.set("data.companyCategory", CompanyData.Companies.get(this.companyName).getCategoryName());
		}
		this.saveConfig();
		PlayerData.playersData.remove(this.player.getName());
	}
	

	/*public BigDecimal getMoney() {
		User user = Main.essentials.getUser(this.player);
		if(user != null)
			return user.getMoney();
		return null;
	}
	
	public boolean setMoney(BigDecimal value) {
		User user = Main.essentials.getUser(this.player);
		if(user != null)
		{
			try {
				user.setMoney(value);
			} catch (MaxMoneyException e) {
				e.printStackTrace();
				return false;
			}
			return true;
		}
		return false;
	}*/
	
	/**
	 * Permet d'obtenir une instance des donn�es du joueur
	 * @param username Pseudo du joueur
	 * @return
	 */
	
	public static PlayerData getPlayerData(String username) {
		if(playersData.containsKey(username))
		{
			return playersData.get(username);
		}
		return null;
	}
	
	public static HashMap<String, PlayerData> getHashMap() {
		return playersData;
	}
	
	public static void SaveDatas() {
		for(Map.Entry<String, PlayerData> entry : PlayerData.getHashMap().entrySet())
		{
			if(entry != null)
				entry.getValue().saveData();
		}
	}
	
	
	
}
