package fr.karmaowner.events;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.FurnaceExtractEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;

import fr.karmaowner.companies.CompanyMenuiserie;
import fr.karmaowner.companies.Company.XP;
import fr.karmaowner.companies.CompanyForgeron;
import fr.karmaowner.companies.CompanyForgeron.XP_FORGERON;
import fr.karmaowner.data.CompanyData;
import fr.karmaowner.data.PlayerData;
import net.md_5.bungee.api.ChatColor;

public class CompanyForgeronEvents implements Listener {
	@EventHandler
	public void	transformItems(InventoryClickEvent e) {
		Player p = (Player)e.getWhoClicked();
		PlayerData Player = PlayerData.getPlayerData(p.getName());
		CompanyData company = CompanyData.Companies.get(Player.companyName);
		if(company != null) {
				if(company.getCompany() instanceof CompanyForgeron) {
					// Si la r�gion lui appartient, on lui emp�che d'exercer son activit�
					if(!company.getCompany().isThatRegion) {
						// si c'est un four
						if(e.getInventory().getType() == InventoryType.FURNACE) {
							CompanyForgeron cf = (CompanyForgeron)company.getCompany();
							// clic gauche
							if(e.isLeftClick() && e.getSlot() == 2) {
								ItemStack recipe = e.getInventory().getContents()[0];
								Material r = e.getInventory().getContents()[2] != null ? e.getInventory().getContents()[2].getType() : null;
								byte data = e.getInventory().getContents()[2] != null ? e.getInventory().getContents()[2].getData().getData() : 0;
								XP item = cf.toXP(r, data);
								// S'il y a un objet
								if(r != null) {
									// si l'objet existe dans la liste
									if(item != null) {
										// si le joueur l'a d�bloqu�
										if(cf.isItemUnlocked(item)) {
											cf.setXp(p, item);
											cf.setCompanyAchievements();
										}else {
											e.setCurrentItem(recipe);
											e.getInventory().setItem(0, new ItemStack(Material.AIR));
											cf.locked_Message(p, item);
										}
									}
								}
							}
						}
					}
				}
		}
	}
	
	@EventHandler
	public void removeXpFromItem(FurnaceExtractEvent e) {
		e.setExpToDrop(0);
	}
}
