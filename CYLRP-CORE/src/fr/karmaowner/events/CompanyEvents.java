package fr.karmaowner.events;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.Inventory;

import fr.karmaowner.common.Main;
import fr.karmaowner.companies.Company;
import fr.karmaowner.companies.CompanyForgeron;
import fr.karmaowner.companies.CompanyMenuiserie;
import fr.karmaowner.data.CompanyData;
import fr.karmaowner.data.PlayerData;
import net.md_5.bungee.api.ChatColor;

import com.sk89q.worldguard.LocalPlayer;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.bukkit.event.block.BreakBlockEvent;
import com.sk89q.worldguard.bukkit.event.block.PlaceBlockEvent;
import com.sk89q.worldguard.bukkit.listener.BuildPermissionListener;
import com.sk89q.worldguard.bukkit.listener.RegionProtectionListener;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class CompanyEvents implements Listener{
	@EventHandler
		public void onPlayerMove(PlayerMoveEvent e) {
			CompanyData cd = CompanyData.Companies.get(PlayerData.getPlayerData(e.getPlayer().getName()).companyName);
			// s'il a une entreprise, on v�rifie qu'il est dans sa propre r�gion pour ceser d'exercer son activit�
			if(cd != null) {
				if(cd.getCompany() != null) {
					cd.getCompany().isThatRegion = false;
					for(ProtectedRegion r : Main.WG.getRegionManager(e.getPlayer().getWorld()).getApplicableRegions(e.getTo())) {
						LocalPlayer lp = Main.WG.wrapPlayer(e.getPlayer());
						if(r.isMember(lp) || r.isOwner(lp)) {
							cd.getCompany().isThatRegion = true;
							return;
						}
					}
				}
			}
		}
	
		@EventHandler
		public void	AgreeOpenCraftingTable(InventoryOpenEvent e) {
			Player p = (Player)e.getPlayer();
			Inventory inv = e.getInventory();
			PlayerData Player = PlayerData.getPlayerData(p.getName());
			CompanyData company = CompanyData.Companies.get(Player.companyName);
			if(company != null) {
				if( !(company.getCompany() instanceof CompanyMenuiserie) ) {
					if(inv.getType().equals(InventoryType.WORKBENCH)) {
						p.sendMessage(ChatColor.DARK_RED+"Vous n'�tes pas autoris� � ouvrir la table de craft.");
						e.setCancelled(true);
					}
				}else {
					// v�rifier que l'inventaire n'est pas visualis� par quelqu'un
					if(false) {
						p.sendMessage(ChatColor.DARK_RED+"Quelqu'un est d�j� en train d'utiliser cette table de craft. Attendez votre tour...");
						e.setCancelled(true);
					}
				}
			}
		}
		
		@EventHandler
		public void	AgreeOpenFurnaceTable(InventoryOpenEvent e) {
			Player p = (Player)e.getPlayer();
			Inventory inv = e.getInventory();
			PlayerData Player = PlayerData.getPlayerData(p.getName());
			CompanyData company = CompanyData.Companies.get(Player.companyName);
			if(company != null) {
				if( !(company.getCompany() instanceof CompanyForgeron) ) {
					if(inv.getType().equals(InventoryType.FURNACE)) {
						p.sendMessage(ChatColor.DARK_RED+"Vous n'�tes pas autoris� � ouvrir le four.");
						e.setCancelled(true);
					}
				}else {
					// v�rifier que l'inventaire n'est pas visualis� par quelqu'un
					if(false) {
						p.sendMessage(ChatColor.DARK_RED+"Quelqu'un est d�j� en train d'utiliser cette table de craft. Attendez votre tour...");
						e.setCancelled(true);
					}
				}
			}
		}
}
