package fr.karmaowner.events;

import org.bukkit.Bukkit;
import org.bukkit.CropState;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerItemBreakEvent;
import org.bukkit.material.Crops;

import fr.karmaowner.companies.Company.XP;
import fr.karmaowner.companies.CompanyAgriculture;
import fr.karmaowner.companies.CompanyBucheron;
import fr.karmaowner.companies.CompanyForgeron;
import fr.karmaowner.data.CompanyData;
import fr.karmaowner.data.PlayerData;
import net.md_5.bungee.api.ChatColor;

public class CompanyAgricultureEvents implements Listener {
	@EventHandler
	public void BlockBreakEvent(BlockBreakEvent e) {
		Player p = e.getPlayer();
		CompanyData data = CompanyData.Companies.get(PlayerData.getPlayerData(p.getName()).companyName);
		if(data != null) {
			if(data.getCompany() != null) {
				if(data.getCompany() instanceof  CompanyAgriculture) {
					// Si la r�gion lui appartient, on lui emp�che d'exercer son activit�
					if(!data.getCompany().isThatRegion) {
						CompanyAgriculture ca = ( CompanyAgriculture)data.getCompany();
						Material itemInHand = p.getItemInHand().getType();
						Block block = e.getBlock();
						Material brokenBlock = block.getType();
						byte getdata = block.getData();
						XP item = ca.toXP(brokenBlock, getdata);
						Bukkit.broadcastMessage(""+brokenBlock);
						int id = block.getTypeId();
						item = item == null ? ca.toXP(id, getdata) : item;
						// Si l'objet existe bien dans la liste
						if(item != null) {
							if(block != null) {
								Crops c = (Crops)block;
								// si l'objet qui est cass� est une graine
								if(c != null) {
									Bukkit.broadcastMessage(""+c.getState());
									// Si la graine est assez m�r pour �tre r�colt�
									if(c.getState() == CropState.RIPE) {
										if(itemInHand == Material.AIR) {
											// si d�bloqu�
											if(ca.isItemUnlocked(item)) {
												ca.setXp(p, item);
												ca.setCompanyAchievements();
											}else {
												ca.locked_Message(p, item);
												e.setCancelled(true);
											}
										}else {
											p.sendMessage(ChatColor.DARK_RED+" Vous ne pouvez r�colter qu'avec les mains");
											e.setCancelled(true);
										}
									}else {
										p.sendMessage(ChatColor.DARK_RED+item.getName()+" n'est pas encore m�r. Veuillez patienter...");
										e.setCancelled(true);
									}
								}
							}
						}
					}
				}
			}
		}
	}
	
	
}
