package fr.karmaowner.events;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import fr.karmaowner.casino.Casino;
import fr.karmaowner.casino.Casino.State;
import fr.karmaowner.common.Main;
import fr.karmaowner.common.TaskCreator;
import fr.karmaowner.data.PlayerData;

public abstract class CasinoEvents implements Listener {
	abstract void onInventoryClick(InventoryClickEvent event);
	
	@EventHandler
	public void onQuit(PlayerQuitEvent e) {
		Player p = e.getPlayer();
		Casino c = Casino.getGame(p);
		if(c != null) {
			if(c.s == State.STARTED) {
				if(c.nbPlayers == 1) {
					Casino.waitingGames.remove(c);
				}else if(c.nbPlayers > 1) {
					c.listPlayers.remove(p);
					c.NotificatePlayers("Un joueur a quitt� la partie "+c.listPlayers.size()+"/"+c.nbPlayers);
				}
			}else if(c.s == State.WAITING) {
				Casino.StopWaiting(p);
			}
		}
	}
}
