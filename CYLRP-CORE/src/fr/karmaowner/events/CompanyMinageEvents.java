package fr.karmaowner.events;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

import fr.karmaowner.companies.Company.XP;
import fr.karmaowner.companies.CompanyBucheron;
import fr.karmaowner.companies.CompanyMinage;
import fr.karmaowner.companies.CompanyMinage.XP_MINAGE;
import fr.karmaowner.data.CompanyData;
import fr.karmaowner.data.PlayerData;
import net.md_5.bungee.api.ChatColor;

public class CompanyMinageEvents implements Listener{
	@EventHandler
	public void BlockBreakEvent(BlockBreakEvent e) {
		Player p = e.getPlayer();
		Block brokenBlock = e.getBlock();
		CompanyData data = CompanyData.Companies.get(PlayerData.getPlayerData(p.getName()).companyName);
		Material itemInHand = p.getItemInHand().getType();
		if(data != null) {
			if(data.getCompany() != null) {
				if(data.getCompany() instanceof CompanyMinage) {
					// Si la r�gion lui appartient, on lui emp�che d'exercer son activit�
					if(!data.getCompany().isThatRegion) {
						CompanyMinage cm = (CompanyMinage)data.getCompany();
						XP itemInHandXP = cm.toXP(itemInHand);
						XP brokenBlockXP = cm.toXP(brokenBlock.getType());
						// Si la pioche est d�bloqu� et que c'est bien une pioche
						if(cm.isItemUnlocked(itemInHandXP) && cm.isPickaxe(itemInHand)) {
							// si le bloc est bien existant dans notre liste de bloc autoris� a �tre cass�
							if(brokenBlockXP != null) {
								// si le bloc est d�bloqu�
								if(cm.isItemUnlocked(brokenBlockXP)) {
									// s'il n'est pas minable
									if(!((XP_MINAGE)brokenBlockXP).getMinable()) {
										e.getBlock().setType(((XP_MINAGE)brokenBlockXP).getType());
									}
									cm.setXp(p, brokenBlockXP);
									cm.setCompanyAchievements();
								}else {
									cm.locked_Message(p, brokenBlockXP);
									e.setCancelled(true);
								}
							}else {
								p.sendMessage("Ce bloc n'est pas cassable.");
								e.setCancelled(true);
							}
						}else {
							// si l'objet existe bien dans la liste
							if(itemInHandXP != null && !cm.isPickaxe(itemInHand)) p.sendMessage("Vous ne pouvez pas casser ce bloc sans pioche");
							e.setCancelled(true);
						}
					}
				}
			}
		}
	}
}
