package fr.karmaowner.events;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.CraftingInventory;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import fr.karmaowner.companies.CompanyMenuiserie;
import fr.karmaowner.companies.Company.XP;
import fr.karmaowner.companies.CompanyBucheron.XP_BUCHERON;
import fr.karmaowner.companies.CompanyMenuiserie.XP_MENUISERIE;
import fr.karmaowner.data.CompanyData;
import fr.karmaowner.data.PlayerData;
import net.md_5.bungee.api.ChatColor;

public class CompanyMenuiserieEvents implements Listener{
	@EventHandler
	public void	CraftingItems(CraftItemEvent e) {
		Player p = (Player)e.getWhoClicked();
		PlayerData Player = PlayerData.getPlayerData(p.getName());
		CompanyData company = CompanyData.Companies.get(Player.companyName);
		if(company != null) {
				if(company.getCompany() instanceof CompanyMenuiserie) {
					// Si la r�gion lui appartient, on lui emp�che d'exercer son activit�
					if(!company.getCompany().isThatRegion) {
						CompanyMenuiserie cm = (CompanyMenuiserie)company.getCompany();
						Material r = e.getRecipe().getResult().getType();
						int id = e.getRecipe().getResult().getTypeId();
						byte data = e.getRecipe().getResult().getData().getData();
						XP item = cm.toXP(r, data);
						item = item == null ? cm.toXP(id, data) : item;
						// si l'objet existe dans la liste
						if(item != null) {
							if(cm.isItemUnlocked(item)) {
								if(e.getAction().equals(InventoryAction.PICKUP_ALL)) {
											cm.setXp(p, item);
											cm.setCompanyAchievements();
								}else if(e.getAction().equals(InventoryAction.MOVE_TO_OTHER_INVENTORY)) {
											e.setCancelled(true);
								}
							}else {
								cm.locked_Message(p, item);
								e.setCancelled(true);
							}
						}else {
							e.setCancelled(true);
							p.sendMessage(ChatColor.DARK_RED+"Vous ne pouvez pas fabriquer cet objet.");
						}
					}
				}
		}
	}
}
