package fr.karmaowner.events;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerFishEvent;

import fr.karmaowner.companies.Company.XP;
import fr.karmaowner.companies.CompanyForgeron;
import fr.karmaowner.companies.CompanyPeche;
import fr.karmaowner.data.CompanyData;
import fr.karmaowner.data.PlayerData;
import net.md_5.bungee.api.ChatColor;

public class CompanyPecheEvents implements Listener {
	@EventHandler
	public void PlayerFish(PlayerFishEvent e) {
		Player p = e.getPlayer();
		PlayerData Player = PlayerData.getPlayerData(p.getName());
		CompanyData company = CompanyData.Companies.get(Player.companyName);
		if(company != null) {
			if(company.getCompany() instanceof CompanyPeche) {
				// Si la r�gion lui appartient, on lui emp�che d'exercer son activit�
				if(!company.getCompany().isThatRegion) {
					CompanyPeche cp = (CompanyPeche)company.getCompany();
					if(e.getState() == PlayerFishEvent.State.CAUGHT_FISH) {
						Material mat = ((Item)e.getCaught()).getItemStack().getType();
						byte data = ((Item)e.getCaught()).getItemStack().getData().getData();
						XP item = cp.toXP(mat, data);
						if(cp.isItemUnlocked(item)) {
							cp.setXp(p, item);
							cp.setCompanyAchievements();
						}else {
							cp.locked_Message(p, item);
							e.setCancelled(true);
						}
					}else if(e.getState() == PlayerFishEvent.State.FISHING) {
						p.sendMessage(ChatColor.GOLD+"P�che en cours...");
					}
					Bukkit.broadcastMessage(""+e.getState());
				}
				
			}
		}
	}
}
