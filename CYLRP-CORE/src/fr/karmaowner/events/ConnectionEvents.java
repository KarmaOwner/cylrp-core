package fr.karmaowner.events;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;


import fr.karmaowner.data.CompanyData;
import fr.karmaowner.data.PlayerData;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class ConnectionEvents implements Listener {
	
	/*
	 *  Lors de la connexion du joueur on cr�e une instance du joueur
	 *  dans la classe PlayerData pour sauvegarder des donn�es
	 */
	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		new PlayerData(e.getPlayer());
	}
	
	@EventHandler
	public void onQuit(PlayerQuitEvent e) {
		PlayerData.getPlayerData(e.getPlayer().getName()).saveData();
	}
}
