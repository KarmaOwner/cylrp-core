package fr.karmaowner.events;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

import fr.karmaowner.companies.Company.XP;
import fr.karmaowner.companies.CompanyBucheron;
import fr.karmaowner.companies.CompanyBucheron.XP_BUCHERON;
import fr.karmaowner.data.CompanyData;
import fr.karmaowner.data.PlayerData;
import net.md_5.bungee.api.ChatColor;

public class CompanyBucheronEvents implements Listener{

	@EventHandler
	public void BlockBreakEvent(BlockBreakEvent e) {
		Player p = e.getPlayer();
		Block brokenBlock = e.getBlock();
		CompanyData data = CompanyData.Companies.get(PlayerData.getPlayerData(p.getName()).companyName);
		Material itemInHand = p.getItemInHand().getType();
		if(data != null) {
			if(data.getCompany() != null) {
				if(data.getCompany() instanceof CompanyBucheron) {
					// Si la r�gion lui appartient, on lui emp�che d'exercer son activit�
					if(!data.getCompany().isThatRegion) {
						CompanyBucheron cb = (CompanyBucheron)data.getCompany();
						XP brokenBlockXP = cb.toXP(brokenBlock.getType(),brokenBlock.getData());
						XP itemInHandXP = cb.toXP(itemInHand);
						if(brokenBlockXP != null){
							if(!cb.isItemUnlocked(brokenBlockXP)) {
									cb.locked_Message(p, brokenBlockXP);
									e.setCancelled(true);
									return;
							}
						}
						if(itemInHandXP != null && cb.isAxe(itemInHand)) {
								if(cb.isItemUnlocked(itemInHandXP)) {
									if(brokenBlockXP != null) {
										cb.setXp(p, brokenBlockXP);
										cb.setCompanyAchievements();
									}
								}else {
									cb.locked_Message(p, itemInHandXP);
									e.setCancelled(true);
								}
						}else {
							p.sendMessage("Vous ne pouvez effectuer cette action sans hache.");
							e.setCancelled(true);
						}
					}
				}
			}
		}
	}
}
