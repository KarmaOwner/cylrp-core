package fr.karmaowner.time;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import java.util.List;
import fr.karmaowner.common.Main;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitScheduler;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.scheduler.BukkitWorker;
import java.util.Date;
public class TimeRunnable extends BukkitRunnable {
	
	private JavaPlugin plugin;
	
	public TimeRunnable(JavaPlugin plugin) {
		this.plugin = plugin;
	}
	
	public void run() {
		Date date = new Date();
		int hours = date.getHours();
		int minutes = date.getMinutes();
		long time = ((hours-6)*1000L)+(minutes*17L);
		plugin.getServer().getWorld(Main.WORLDNAME).setTime(time%24000L);
	}

}