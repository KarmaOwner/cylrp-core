package fr.karmaowner.jobs;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;

import fr.karmaowner.common.JobsRegistery;
import fr.karmaowner.common.Main;

public class Jobs implements Listener {
	
	public ItemStack item;
	public List<String> description = new ArrayList();
	
	private String name;
	
	public String permission = "";
	
	public boolean isDefault = false;
	
	/**
	 * 
	 * @param item ItemStack affichage de l'item dans l'interface
	 * @param name Nom du m�tier
	 * @param description Description associ� au m�tier
	 * @param permission Permission associ� au m�tier
	 * @param isDefault Le m�tier est-il par d�faut?
	 */
	public Jobs(ItemStack item, String name, List<String> description, String permission, boolean isDefault) {
		this.item = item;
		this.name = name;
		this.description = description;
		this.permission = permission;
		this.isDefault = isDefault;
		
		if(item != null) {
			item.getItemMeta().setLore(description);
		}
		
		Main.INSTANCE.getServer().getPluginManager().registerEvents(this, Main.INSTANCE);
	}
	
	/**
	 * 
	 * @return le m�tier par defaut
	 */
	public static Jobs getDefaultJob() {
		for(Jobs job : JobsRegistery.registeredJobs)
		{
			if(job.isDefault)
				return job;
		}
		return null;
	}
	
	/**
	 * 
	 * @return le nom du m�tier
	 */
	public String getName() {
		return name;
	}
	
	
	
	
}
