package fr.karmaowner.utils;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import fr.karmaowner.common.Main;

public enum MoneyTicket {
 
	ONEDOLLARS,
	FIVEDOLLARS,
	TENDOLLARS,
	HUNDREDDOLLARS,
	THOUSANDDOLLARS;
	
	@SuppressWarnings("deprecation")
	public static ItemStack toItem(MoneyTicket value) 
	{
		if(value == ONEDOLLARS)
		{
			return new ItemStack(Material.getMaterial(Main.INSTANCE.getConfig().getInt("tickets.1dollars.id")), 1, (byte) Main.INSTANCE.getConfig().getInt("tickets.1dollars.byte"));
		}
		else if(value == FIVEDOLLARS)
		{
			return new ItemStack(Material.getMaterial(Main.INSTANCE.getConfig().getInt("tickets.5dollars.id")), 1, (byte) Main.INSTANCE.getConfig().getInt("tickets.5dollars.byte"));
		}
		else if(value == TENDOLLARS)
		{
			return new ItemStack(Material.getMaterial(Main.INSTANCE.getConfig().getInt("tickets.10dollars.id")), 1, (byte) Main.INSTANCE.getConfig().getInt("tickets.10dollars.byte"));
		}
		else if(value == HUNDREDDOLLARS)
		{
			return new ItemStack(Material.getMaterial(Main.INSTANCE.getConfig().getInt("tickets.100dollars.id")), 1, (byte) Main.INSTANCE.getConfig().getInt("tickets.100dollars.byte"));
		}
		else if(value == THOUSANDDOLLARS)
		{
			return new ItemStack(Material.getMaterial(Main.INSTANCE.getConfig().getInt("tickets.1000dollars.id")), 1, (byte) Main.INSTANCE.getConfig().getInt("tickets.1000dollars.byte"));
		}
		return null;
	}
	
	@SuppressWarnings("deprecation")
	public static MoneyTicket toMoneyTicket(ItemStack item)
	{
		if(item.isSimilar(new ItemStack(Material.getMaterial(Main.INSTANCE.getConfig().getInt("tickets.1dollars.id")), 1, (byte) Main.INSTANCE.getConfig().getInt("tickets.1dollars.byte"))))
		{
			return ONEDOLLARS;
		}
		else if(item.isSimilar(new ItemStack(Material.getMaterial(Main.INSTANCE.getConfig().getInt("tickets.5dollars.id")), 1, (byte) Main.INSTANCE.getConfig().getInt("tickets.5dollars.byte"))))
		{
			return FIVEDOLLARS;
		}
		else if(item.isSimilar(new ItemStack(Material.getMaterial(Main.INSTANCE.getConfig().getInt("tickets.10dollars.id")), 1, (byte) Main.INSTANCE.getConfig().getInt("tickets.10dollars.byte"))))
		{
			return TENDOLLARS;
		}
		else if(item.isSimilar(new ItemStack(Material.getMaterial(Main.INSTANCE.getConfig().getInt("tickets.100dollars.id")), 1, (byte) Main.INSTANCE.getConfig().getInt("tickets.100dollars.byte"))))
		{
			return HUNDREDDOLLARS;
		}
		else if(item.isSimilar(new ItemStack(Material.getMaterial(Main.INSTANCE.getConfig().getInt("tickets.1000dollars.id")), 1, (byte) Main.INSTANCE.getConfig().getInt("tickets.1000dollars.byte"))))
		{
			return THOUSANDDOLLARS;
		}
		return null;
	}
	
	public static int toInteger(MoneyTicket value)
	{
		if(value == ONEDOLLARS)
		{
			return 1;
		}
		else if(value == FIVEDOLLARS)
		{
			return 5;
		}
		else if(value == TENDOLLARS)
		{
			return 10;
		}
		else if(value == HUNDREDDOLLARS)
		{
			return 100;
		}
		else if(value == THOUSANDDOLLARS)
		{
			return 1000;
		}
		return 0;
	}
	
	
}
