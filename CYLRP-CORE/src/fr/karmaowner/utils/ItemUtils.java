package fr.karmaowner.utils;

import java.util.List;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;

public class ItemUtils {

	@SuppressWarnings("deprecation")
	public static ItemStack fromId(String id, List<String> description, String name, int quantity) {
		int itemId;
		byte material = 0;
		
		if(id.contains(":")) {
			String[] ids = id.split(":");
			itemId = Integer.parseInt(ids[0]);
			material = (byte) Integer.parseInt(ids[1]);
		}
		else
			itemId = Integer.parseInt(id);
			
		
		if(description != null || name != null)
		{
			ItemStack it = new ItemStack(Material.getMaterial(itemId), quantity, material);
			ItemMeta itMeta = it.getItemMeta();
			if(description != null)
				itMeta.setLore(description);
			if(name != null)
				itMeta.setDisplayName(name);
			return it;
		}
		else 
			return new ItemStack(Material.getMaterial(itemId),quantity, material);
	
	}
	
	public static void addItem(ItemStack item, Player p) 
	{
		PlayerInventory pInventory = p.getInventory();
		pInventory.addItem(item);
	}
	
}
