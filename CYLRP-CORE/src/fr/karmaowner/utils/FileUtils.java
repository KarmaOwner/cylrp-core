package fr.karmaowner.utils;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.configuration.file.FileConfiguration;

import fr.karmaowner.common.Main;

public class FileUtils {
	
	protected File folderdirectory;
	protected File filedirectory;
	
	protected FileConfiguration fileConfiguration;
	
	protected Player player;
	
	/**
	 * 
	 * @param p Pseudo du joueur
	 */
	public FileUtils(Player p) {
		player = p;
		folderdirectory = new File(Main.INSTANCE.getDataFolder() + File.separator + "UserData");
		filedirectory = new File(Main.INSTANCE.getDataFolder() + File.separator + "UserData" + File.separator + player.getName() + ".yml");
	}
	
	public FileConfiguration getFileConfiguration() {
		return fileConfiguration;
	}
	
	public FileUtils(String fileName, String directoryName) {
		folderdirectory = new File(Main.INSTANCE.getDataFolder() + File.separator + directoryName);
		filedirectory = new File(Main.INSTANCE.getDataFolder() + File.separator + directoryName + File.separator + fileName + ".yml");
	}

	/*** permet de cr�er le fichier username.yml ***/
	public void createFile() {
		if(directoryExist())
			return;
		
		try {
			try {
				folderdirectory.mkdir();
				filedirectory.createNewFile();
			} catch (IOException e) {
				Main.Log("Erreur lors de la cr�ation du fichier");
				e.printStackTrace();
			}
		}
		catch(SecurityException e) {
			Main.Log("Erreur lors de la cr�ation de " + filedirectory.getName() + ".yml d�sactivation du plugin");
			Main.Log(e.toString());
			Main.INSTANCE.getServer().getPluginManager().disablePlugin(Main.INSTANCE);
		}
	}
	
	public void deleteFile() {
		if(directoryExist()) {
			Main.Log("Fich� " + filedirectory.getName() + "supprim�");
			filedirectory.delete();
		}
	}
	
	/*** Charge le fichier yml ***/
	public void loadFileConfiguration() {
		fileConfiguration = YamlConfiguration.loadConfiguration(filedirectory);
	}
	
	/*** 
	 retourne vrai si le fichier username.yml existe
	 faux sinon 	 	 
	***/
	public boolean directoryExist() {
		if(filedirectory.exists())
			return true;
		return false;
	}
	

	/*** Sauvegarde du fichier username.yml ***/
	public void saveConfig() {
		try {
			fileConfiguration.save(filedirectory);
		} catch (IOException e) {
			Main.Log("Erreur lors de la sauvegarde du fichier " + filedirectory.getName());
			e.printStackTrace();
		}
	}
	

}
