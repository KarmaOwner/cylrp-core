package fr.karmaowner.utils;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import fr.karmaowner.common.Main;

public class MoneyUtils {
	
	public static int convertTicketsToValue(ItemStack tickets) {
		int quantity = tickets.getAmount();
		int a = 0;
		for(int i = 0; i < quantity; i++)
		{
			MoneyTicket value = MoneyTicket.toMoneyTicket(tickets);
			a += MoneyTicket.toInteger(value);
		}
		return a;
	}
	
	public static boolean addMoneyToBank(int value, Player p) {
		int oneDollars = 0, fiveDollars = 0 , tenDollars = 0 , hundredDollars = 0 ,thousandDollars = 0;
		while(value > 0) {
			if((value -= 1000) > 0)
				thousandDollars++;
			else if((value -= 100) > 0)
				hundredDollars++;
			else if((value -= 10) > 0)
				tenDollars++;
			else if((value -= 5) > 0)
				fiveDollars++;
			else
				oneDollars++;
		}
		
		ItemStack oneDollarsIS = new ItemStack(Material.getMaterial(Main.INSTANCE.getConfig().getInt("tickets.1dollars.id")), oneDollars, (byte) Main.INSTANCE.getConfig().getInt("tickets.1dollars.byte"));
		ItemStack fiveDollarsIS = new ItemStack(Material.getMaterial(Main.INSTANCE.getConfig().getInt("tickets.5dollars.id")), fiveDollars, (byte) Main.INSTANCE.getConfig().getInt("tickets.5dollars.byte"));
		ItemStack tenDollarsIS = new ItemStack(Material.getMaterial(Main.INSTANCE.getConfig().getInt("tickets.10dollars.id")), tenDollars, (byte) Main.INSTANCE.getConfig().getInt("tickets.10dollars.byte"));
		ItemStack hundredDollarsIS = new ItemStack(Material.getMaterial(Main.INSTANCE.getConfig().getInt("tickets.100dollars.id")), hundredDollars, (byte) Main.INSTANCE.getConfig().getInt("tickets.100dollars.byte"));
		ItemStack thousandDollarsIS = new ItemStack(Material.getMaterial(Main.INSTANCE.getConfig().getInt("tickets.1000dollars.id")), thousandDollars, (byte) Main.INSTANCE.getConfig().getInt("tickets.1000dollars.byte"));		
		
		PlayerInventory pInventory = p.getInventory();
		
		if(pInventory.contains(oneDollarsIS) && pInventory.contains(fiveDollarsIS) && pInventory.contains(tenDollarsIS) && pInventory.contains(hundredDollarsIS) && pInventory.contains(thousandDollarsIS))
			return true;
		
		
		return false;
		
	}
	
	@SuppressWarnings("deprecation")
	public static void convertValueToTickets(int value, Player p)
	{
		int oneDollars = 0, fiveDollars = 0 , tenDollars = 0 , hundredDollars = 0 ,thousandDollars = 0;
		while(value > 0) {
			if((value -= 1000) > 0)
				thousandDollars++;
			else if((value -= 100) > 0)
				hundredDollars++;
			else if((value -= 10) > 0)
				tenDollars++;
			else if((value -= 5) > 0)
				fiveDollars++;
			else
				oneDollars++;
		}
		
		ItemUtils.addItem(new ItemStack(Material.getMaterial(Main.INSTANCE.getConfig().getInt("tickets.1dollars.id")), oneDollars, (byte) Main.INSTANCE.getConfig().getInt("tickets.1dollars.byte")), p);
		ItemUtils.addItem(new ItemStack(Material.getMaterial(Main.INSTANCE.getConfig().getInt("tickets.5dollars.id")), fiveDollars, (byte) Main.INSTANCE.getConfig().getInt("tickets.5dollars.byte")), p);
		ItemUtils.addItem(new ItemStack(Material.getMaterial(Main.INSTANCE.getConfig().getInt("tickets.10dollars.id")), tenDollars, (byte) Main.INSTANCE.getConfig().getInt("tickets.10dollars.byte")), p);
		ItemUtils.addItem(new ItemStack(Material.getMaterial(Main.INSTANCE.getConfig().getInt("tickets.100dollars.id")), hundredDollars, (byte) Main.INSTANCE.getConfig().getInt("tickets.100dollars.byte")), p);
		ItemUtils.addItem(new ItemStack(Material.getMaterial(Main.INSTANCE.getConfig().getInt("tickets.1000dollars.id")), thousandDollars, (byte) Main.INSTANCE.getConfig().getInt("tickets.1000dollars.byte")), p);		
	}
	
}
